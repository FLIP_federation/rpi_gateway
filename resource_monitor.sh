#!/bin/bash


while true
do
	now="$(date +'%d_%m_%Y_%H:%M:%S')"
	MEM="$(awk '/MemTotal/{t=$2}/MemAvailable/{a=$2}END{print 100-100*a/t}' /proc/meminfo)"
	CPU="$((grep 'cpu ' /proc/stat;sleep 0.1;grep 'cpu ' /proc/stat)|awk -v RS="" '{print ($13-$2+$15-$4)*100/($13-$2+$15-$4+$16-$5)}')"
	echo "$now,$CPU,$MEM"
	sleep 60
done