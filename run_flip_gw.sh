#!/bin/bash
POSITIONAL=()
while [[ $# -gt 0 ]]
do
case $1 in
  -v|--verbose) # enabling logs for the run
    verbose=1
    shift
    ;;
  -i|--init) # general configuration option
    initi=1
    shift
    ;;
  -c|--cluster) # general configuration option
    cluster="$2"
    shift
    shift
    ;;
  -n|--name) # general configuration option
    name="$2"
    shift
    shift
    ;;
  *) echo "Unknown parameter passed: $1"
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac; done

set -- "${POSITIONAL[@]}" # restore positional parameters

# Init for tinc-boot and FLIPVARS
if [[ -n $initi && -n $cluster && -n $name ]]; then
  curl -L https://github.com/reddec/tinc-boot/releases/latest/download/tinc-boot_linux_armv7.tar.gz | sudo tar -xz -C /usr/local/bin/ tinc-boot
  sudo tinc-boot gen --name=$name --network=flipvpn --prefix=10.1.$cluster --token "FirstGlobalFLIPfederation" 51.91.9.136:8655
  GW_ID=$(awk '/Subnet/ {print $3}' /etc/tinc/flipvpn/hosts/$name | cut -d. -f4 | cut -d'/' -f1)
  echo "Gateway params: cluster $cluster gateway $GW_ID named $name"
  echo "export GW_CLUSTER=$cluster" >> ./flip.conf
  echo "export GW_ID=$GW_ID" >> ./flip.conf
  sudo systemctl start tinc@flipvpn
  sudo systemctl enable tinc
  sudo systemctl enable tinc@flipvpn
  echo "Successful initialization. Exiting."
  exit 0
fi  


# Sourcing General settings
source flip.conf

if [[ -n $verbose ]]; then
  FLIPLOGPATH=/tmp/FLIP
  echo "Verbose logging in $FLIPLOGPATH ..."
  mkdir -p "$FLIPLOGPATH"
  now="$(date)"
  now="$(date +'%d_%m_%Y_%H:%M:%S')"
  FILE0="${FLIPLOGPATH}/cluster_${GW_CLUSTER}_gw_${GW_ID}_"$now"_packetforwarder.log"
  FILE1="${FLIPLOGPATH}/cluster_${GW_CLUSTER}_gw_${GW_ID}_"$now"_lorawanlayer.log"
  FILE2="${FLIPLOGPATH}/cluster_${GW_CLUSTER}_gw_${GW_ID}_"$now"_fliplayer.log"
  FILE3="${FLIPLOGPATH}/cluster_${GW_CLUSTER}_gw_${GW_ID}_"$now"_network_utilisation.log"
  FILE4="${FLIPLOGPATH}/cluster_${GW_CLUSTER}_gw_${GW_ID}_"$now"_resource_utilisation.log" 
  echo $FILE0
  echo $FILE1
  echo $FILE2
  echo $FILE3
  echo $FILE4
  #### Monitor CPU and RAM usage
  echo "Timestamp , CPU percentage , MEM percentage" > $FILE4
  /home/pi/FLIP/resource_monitor.sh >> $FILE4 &
  echo $! > /home/pi/FLIP/resourcemonitor.pid
else
  FILE0="/dev/null"
  FILE1="/dev/null"
  FILE2="/dev/null"
  FILE3="/dev/null"
  FILE4="/dev/null" 
fi

#### Create the DB of owned end-devices
export ED_DB="owned_eds_cluster_${GW_CLUSTER}_gw_${GW_ID}.db"

sqlite3 $ED_DB <<EOM
CREATE TABLE owned_eds (
  Devaddr TEXT,
  appeui TEXT,
  deveui TEXT,
  appkey TEXT,
  appskey TEXT,
  nwkskey TEXT,
  fcounter TEXT
);
.exit
EOM

# populate the DB with content of ED ownership file

while IFS=, read SERIALID APPEUI DEVEUI APPKEY CLUSTER ID
do
  if [ "$CLUSTER" == "$GW_CLUSTER" ] && [ "$ID" == "$GW_ID" ]
  then
    COMMAND="INSERT INTO owned_eds VALUES(0, '$APPEUI', '$DEVEUI', '$APPKEY', 0, 0, 0);"
    echo $COMMAND
    sqlite3 $ED_DB "$COMMAND"
  else
    echo "Input ownership does not match gateway settings:"
    echo $CLUSTER $" " $GW_CLUSTER
    echo $ID $" " $GW_ID
  fi
done < /home/pi/FLIP/eds.own


#### Wait for the launch of tincVPN
printf "%s" "Bootstrapping VPN ..."

while ! ping -c 1 -n $FLIPVPN_BOOTSTRAP &> /dev/null
do
    printf "%c" "."
done

printf "\n%s\n"  "Successful VPN connection."


#### Operative launch of scripts, executables and logs

export PYTHONPATH=$PYTHONPATH:../

clean() {
  #  kill python instances, clean DB, rm pid_files, maybe even logs ?
  kill `cat pckfwdr.pid`; kill `cat lorawanlayer.pid`; kill `cat fliplayer.pid`; kill `cat tcpdump.pid`; kill `cat networkmonitor.pid`; kill `cat resourcemonitor.pid`;
  rm owned_eds*.db
  rm *.pid
}

trap clean SIGHUP SIGINT SIGTERM 

./lora_gateway/reset_lgw.sh stop 25
sleep 1
./lora_gateway/reset_lgw.sh start 25
sleep 1
echo "LoRa Packet Fwd not started, trying again..."
./packet_forwarder/lora_pkt_fwd/lora_pkt_fwd > $FILE0 2>&1  &
echo $! > pckfwdr.pid
sleep 3
while :
do
 if pgrep -x "lora_pkt_fwd" > /dev/null
 then
   echo "LoRa Packet Fwd is running now"
   break
 else
   ./lora_gateway/reset_lgw.sh stop 25
   sleep 1
   ./lora_gateway/reset_lgw.sh start 25
   sleep 1
   echo "LoRa Packet Fwd not started, trying again..."
   ./packet_forwarder/lora_pkt_fwd/lora_pkt_fwd > $FILE0 2>&1  &
   echo $! > pckfwdr.pid
   sleep 3
 fi
done

export PYTHONPATH=$(dirname $0)
python3 -u ./lorawan_layer/lorawan_dispatcher.py > $FILE1  &
echo $! > lorawanlayer.pid

python3 -u ./flip_layer/gateway.py > $FILE2 &
echo $! > fliplayer.pid

( tcpdump -i $FLIPIFACE portrange 9000-9005 and not src net $FLIPMONSUBNET/24 and not dst net $FLIPMONSUBNET/24 & echo $! > tcpdump.pid ) 2>&1 |\
    python3 -u network_monitor/network_monitor_client.py > $FILE3 &
echo $! > networkmonitor.pid


echo "Waiting for keyboard interrupt"
sleep infinity
