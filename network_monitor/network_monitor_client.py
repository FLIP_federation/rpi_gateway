#!/usr/bin/python3

import sys
import re
import datetime

import subprocess

o = subprocess.getoutput("ifconfig")
ip = re.search("(10\.1\.\d+\.\d+)", o).group(1)

fro_tot = 0
to_tot = 0

prev_now = datetime.datetime.now()

while 1:
    try:
        line = sys.stdin.readline().rstrip()
    except KeyboardInterrupt:
        break

    result = re.search("(\d+\.\d+\.\d+\.\d+)\.\d+ > (\d+\.\d+\.\d+\.\d+)\.\d+\:.*length\s(\d+)$", line)
    if result is None:
        continue
    fro, to, length = result.group(1), result.group(2), result.group(3)
    if fro == ip:
        fro_tot += int(length)
    elif to == ip:
        to_tot += int(length)
    else:
        print("ERROR: from " + str(fro) + " to " + str(to) + " length " + str(length))


    now = datetime.datetime.now()
    if (now - prev_now).total_seconds() > 5:
        print("[" + now.strftime("%H:%M:%S") + "] Total bytes from me: " + str(fro_tot) + ". Total bytes to me " + str(to_tot))
        prev_now = now
