import queue
import socket
import signal
import struct
import sys
import threading
import asyncio
import base64
import os
import datetime
import config
#sys.path.insert(0, '../')

from flip_layer.logger import logger
import lorawan_layer.lorawan_parser as lorawan_parser
import lorawan_layer.lorawan_socket_handle as lorawan_socket_handle
from lorawan_layer.device_manager import Device_Manager
from lorawan_layer.lorawan_stats import stats
import lorawan_layer.lorawan_socket_handle as com

received_packet = queue.Queue()
send_packet = queue.Queue()
device_manager = Device_Manager()

## Coordinator for LoRaWAN lower layer. Keeps LoRaWAN-related state, launches background tasks and handles communication with
#   upper FLIP layer.
class LoRaWAN_Dispatcher():
    def __init__(self):

        self.lorawan_parser_thread = threading.Thread(name='lorawan parser', target=lorawan_parser.lorawan_parser,
                                                  args=(send_packet, received_packet))
        self.lorawan_parser_thread.daemon = True;

        self.lorawan_asyncio_socket_thread = threading.Thread(name='lorawan parser', target=self.run_asyncio_socket,
                                                 args=())
        self.lorawan_asyncio_socket_thread.daemon = True;

        self.loop = asyncio.get_event_loop()

        ## TCP port at which service is provided to FLIP layer.
        self.lorawan_flip_server_port = config.LORAWAN_SERVER_PORT

        ## TCP port at which service is provided by FLIP layer.
        self.flip_lorawan_server_port = config.FLIP_LOW_SERVER_PORT

        ## TCPListener that provides service to FLIP layer
        self.lorawan_listener = TCPListener(self.loop, config.HOST, self.lorawan_flip_server_port)

        self.device_manager = device_manager



        signal.signal(signal.SIGINT, self.teardown)
        signal.signal(signal.SIGTERM, self.teardown)

    ## Prepares join messages in right format and sends it to FLIP layer.
    #@param dev_obj
    #@param appmsg complete appmsg
    async def send_join_msg(self,dev_obj,appmsg):
        msg = [appmsg, dev_obj.dev_nonce, dev_obj.mic]
        json_new_handler = {
            "ed_id": dev_obj.deveui,
            "lorawan_data": msg,
            "rssi": dev_obj.rssi,
            "channel": dev_obj.channel,
            "sf": dev_obj.sf

        }
        s = await com.FLIPClientSession.obtain(self.loop)
        s.join_request(json_new_handler)
        s.end()
        stats.counters['join_messages_sent_to_flip_layer']+=1
        logger.log("LoRaWAN dispatcher", "A join Message passed to FLIP layer")


    ## Prepares uplink messages in right format and sends it to FLIP layer.
    #@param dev_obj
    #@param appmsg complete appmsg
    async def send_uplink_msg(self,dev_obj,appmsg):
        # try:
        #     msg = [base64.b64encode(appmsg),base64.b64encode(dev_obj.seq_nbr)]
        # except:
        #     try:
        #         msg = [appmsg, dev_obj.seq_nbr]
        #     except:
        #         logger.log("Message dispatcher(send_uplink_msg)", "Message couldn't get decoded(Decoding Error)")

        msg = [str(base64.encodebytes(appmsg),'utf-8'),dev_obj.seq_nbr]

        json_new_handler = {
            "ed_id": dev_obj.deveui,
            "lorawan_data": msg,

        }
        s = await com.FLIPClientSession.obtain(self.loop)
        s.device_data_for_owner(json_new_handler)
        s.end()
        stats.counters['uplink_messages_sent_to_flip_layer']+=1
        logger.log("LoRaWAN dispatcher", "An Uplink Message passed to FLIP layer")


    ## Runs the loop for asynchronus socket communication with FLIP layer.
    def run_asyncio_socket(self):
        try:
            self.loop.run_forever()
        except KeyboardInterrupt:
            sys.exit()
        finally:
            self.teardown()
            self.loop.close()
            sys.exit()

    ## Sets listener threads and stars parser thread and main thread to manage whole lorawan layer.
    def run(self):
        asyncio.ensure_future(stats.logging_task())
        asyncio.ensure_future(self.lorawan_listener.listen())
        asyncio.ensure_future(self.device_manager.maintenance_task())
        asyncio.ensure_future(self.main_thread())

        try:
            self.lorawan_parser_thread.start()
            logger.log("LoRaWAN Dispatcher", "Running LoRaWAN Parser ")
        except:
            logger.error("LoRaWAN Dispatcher", "LoRaWAN layer Threading Problem in parser")
            sys.exit()

        # try:
        #     self.lorawan_asyncio_socket_thread.start()
        #     logger.log("LoRaWAN Layer setup", "Running TCP Socket for FLIP Layer")
        # except:
        #     logger.log("LoRaWAN Layer setup", "TCP Socket Problem in LoRaWAN layer")
        #     sys.exit()
        self.run_asyncio_socket()

    ## Main thread of LoRaWAN dispatcher.
    #Receives messages from lorawan_parser, checks MIC validation, distribute messages between layers depending on message type.
    async def main_thread(self):
        while(True):
            if not received_packet.empty():
                #logger.log("LoRaWAN Dispatcher", "get info from queue")
                msg = received_packet.get()
                received_packet.task_done()
                dev_info = received_packet.get()
                received_packet.task_done()
                #logger.log("LoRaWAN Dispatcher", "queue job is done")

                logger.log("LoRaWAN Dispatcher", "A Message Received from Parser, the message will be checked now (MIC check and Dev Type)")

                msg_type,device_type,dev_obj,appmsg,mic_check = self.check_message_type_and_device(msg,dev_info)

                if mic_check:
                    log_str = str(" deveui:"+str(dev_obj.deveui) + " devaddr:" + str(dev_obj.devaddr) + " channel:" + str(dev_obj.channel) + " rssi:" + str(dev_obj.rssi) + " snr:"+ str(dev_obj.snr)+
                            " handle:" + str(dev_obj.handle) + " owned:" + str(dev_obj.owned) )
                    if msg_type == 'uplink':
                        dev_obj.last_msg_time = datetime.datetime.now()
                        if device_type == 'handle_owned':
                            logger.log("LoRaWAN dispatcher", "An Uplink Message Received From a Handled and Owned Device, info:" + log_str)
                            try:
                             decrypted_payload = self.device_manager.decrypt_payload(dev_obj,appmsg)
                             self.device_manager.save_payload(dev_obj, decrypted_payload)
                            except:
                             logger.error("LoRaWAN Dispatcher", "Uplink couldn't get decrypted")
                            #await self.send_uplink_msg(dev_obj,appmsg)
                        elif device_type == 'handle':
                            logger.log("LoRaWAN Dispatcher", "Uplink Received From a Handled Device, info:" + log_str)
                            await self.send_uplink_msg(dev_obj,appmsg)
                            # pass it to FLIP layer
                            pass
                        else:
                            logger.log("LoRaWAN Dispatcher", "Uplink Received from an Unknown Device, info:" + log_str)
                    elif msg_type == 'join_req':
                        dev_obj.last_msg_time = datetime.datetime.now()
                        if device_type == 'handle_owned':
                            logger.log("LoRaWAN Dispatcher", "Join Req Received From a Handled and Owned Device, info:" + log_str + " dev_nonce:" + str(dev_obj.dev_nonce))
                            await self.send_join_msg(dev_obj,appmsg)
                            # join_ack = self.device_manager.create_join_ack(dev_obj)
                            # send_packet.put(join_ack)
                            # send_packet.join()
                            #update device info
                        elif device_type == 'owned':
                            # pass it to FLIP layer
                            logger.log("LoRaWAN Dispatcher", "Join Req Received From an Owned Device, info:" + log_str + " dev_nonce:" + str(dev_obj.dev_nonce) )
                            await self.send_join_msg(dev_obj,appmsg)

                            pass
                        elif device_type == 'handle':
                            logger.log("LoRaWAN Dispatcher", "Join Req Received From a Handled Device, info:" + log_str + " dev_nonce:" + str(dev_obj.dev_nonce))
                            await self.send_join_msg(dev_obj,appmsg)
                        else:
                            logger.log("LoRaWAN Dispatcher", "Join Req Received From an Unknown Device, info:" + log_str + " dev_nonce:" + str(dev_obj.dev_nonce))
                            await self.send_join_msg(dev_obj, appmsg)
                            # join_ack = self.device_manager.create_join_ack(dev_obj)
                            # send_packet.put(join_ack)
                            # send_packet.join()
                            #request join ack
                            #update device info
                            #send it to the parser
                            pass
                    else:
                        logger.log("LoRaWAN Dispatcher", "Unknown/Unsupported Message Type")

                else:
                    logger.log("LoRaWAN Dispatcher","MIC Fail/Unkown Uplink")
            await asyncio.sleep(0.1)

    ## A helper method for the dispatcher to identift message and device type.
    #@param msg complete received message
    #@param low leven information about received message(rssi,snr, channel,sf)
    #@returns message tpye, device type, device object(if a device is found, appmsg and returns MIC validation
    def check_message_type_and_device(self,msg,dev_info):
        byte_msg = bytearray(msg)
        if byte_msg[0] == 0x40:
            msg_type = 'uplink'
            #rx_dev_addr = (byte_msg[4] << 24) | (byte_msg[3] << 16) | byte_msg[2] << 8 | byte_msg[1]
            seq_nbr = byte_msg[7] << 8 | byte_msg[6]
            appmsg = msg[9:-4]


            devaddr = struct.unpack("!BBBB", memoryview(msg[1:5]))
            id = (devaddr[3] << 24) | (devaddr[2] << 16) | (devaddr[1] << 8) | devaddr[0]

            mote_id = format(id, 'x')
            device_type,dev_obj = self.device_manager.check_device_devaddr(mote_id)
            if device_type == 'unknown_device':
                if(dev_obj == None):
                    self.device_manager.register_unknown_dev_profile(mote_id)
                    device_type, dev_obj = self.device_manager.check_device_devaddr(mote_id)

                dev_obj.sf = dev_info['sf']
                dev_obj.rssi = dev_info['rssi']
                dev_obj.snr = dev_info['snr']
                dev_obj.channel = dev_info['channel']
                dev_obj.devaddr = mote_id
                mic_check = None
                logger.log("LoRaWAN Dispatcher", "Unknown Devaddr")
            else:
                if seq_nbr != dev_obj.seq_nbr:
                    dev_obj.sf = dev_info['sf']
                    dev_obj.rssi = dev_info['rssi']
                    dev_obj.snr = dev_info['snr']
                    dev_obj.channel = dev_info['channel']
                    dev_obj.seq_nbr = seq_nbr
                    dev_obj.mic = byte_msg[-4:]
                    dev_obj.devaddr = mote_id
                    mic_check = self.device_manager.check_mic_uplink_message(dev_obj, byte_msg[1:-4])
                else:
                    mic_check = None
                    logger.log("LoRaWAN Dispatcher", "Old frame counter, possible replayed message")

            logger.log("LoRaWAN Dispatcher", "MIC Check and Dev Type Identification is Done")

            return msg_type,device_type,dev_obj,appmsg,mic_check

        elif byte_msg[0] == 0:
            msg_type = 'join_req'
            app_eui = (byte_msg[8] << 56) | (byte_msg[7] << 48) | (byte_msg[6] << 40) | (byte_msg[5] << 32) | (
            byte_msg[4] << 24) | (byte_msg[3] << 16) | (byte_msg[2] << 8) | byte_msg[1]
            mote_app_eui = format(app_eui, 'x')
            dev_eui = (byte_msg[16] << 56) | (byte_msg[15] << 48) | (byte_msg[14] << 40) | (byte_msg[13] << 32) | (
            byte_msg[12] << 24) | (byte_msg[11] << 16) | (byte_msg[10] << 8) | byte_msg[9]
            mote_dev_eui = format(dev_eui, 'x')
            dev_nonce = (byte_msg[18] << 8) | byte_msg[17]
            mote_dev_nonce = format(dev_nonce, 'x')

            message_mic = (byte_msg[22] << 24) | (byte_msg[21] << 16) | byte_msg[20] << 8 | byte_msg[19]
            mote_message_mic = format(message_mic, 'x')
            device_type,dev_obj = self.device_manager.check_device_deveui(mote_dev_eui)
            #dev_obj.mic = mote_message_mic
            if device_type == 'unknown_device':
                #dev_obj = None
                #mic_check = None
                if dev_obj == None:
                    self.device_manager.register_device(mote_dev_eui,None,None)
                    device_type, dev_obj = self.device_manager.check_device_deveui(mote_dev_eui)

                dev_obj.sf = dev_info['sf']
                dev_obj.rssi = dev_info['rssi']
                dev_obj.snr = dev_info['snr']
                dev_obj.channel = dev_info['channel']
                dev_obj.dev_nonce = dev_nonce
                dev_obj.mic = mote_message_mic
                dev_obj.handle = False
                mic_check = True; #not checking MIC, owner will check
                logger.warn("LoRaWAN Dispatcher", "Skipping MIC Check for Join Req")

            elif device_type == 'handle':
                dev_obj.sf = dev_info['sf']
                dev_obj.rssi = dev_info['rssi']
                dev_obj.snr = dev_info['snr']
                dev_obj.channel = dev_info['channel']
                dev_obj.dev_nonce = dev_nonce
                dev_obj.mic = mote_message_mic
                dev_obj.handle = False
                mic_check = True#mic_check = self.device_manager.check_mic_joinreq(dev_obj, msg, msg_type)#Big issue for join request discuss with SD
                logger.warn("LoRaWAN Dispatcher", "Skipping MIC Check for Join Req")

            else:
                dev_obj.sf = dev_info['sf']
                dev_obj.rssi = dev_info['rssi']
                dev_obj.snr = dev_info['snr']
                dev_obj.channel = dev_info['channel']
                dev_obj.dev_nonce = dev_nonce
                dev_obj.mic = mote_message_mic
                dev_obj.handle = False
                mic_check = self.device_manager.check_mic_joinreq(dev_obj, msg, msg_type)

            logger.log("LoRaWAN Dispatcher", "MIC Check and Dev Type Identification is Done")
            return msg_type,device_type,dev_obj,None,mic_check

        else:
            logger.warn("LoRaWAN Dispatcher Warning", "unsupported message type")

    def teardown(self):
        logger.log("Gateway setup", "Shutting down gateway")
        self.lorawan_listener.teardown()
        sys.exit()

    #def bootstrap(self):



## TCP port listener to provide service to FLIP layer.
# Spawns session.DispatcherSession to handle incoming request.
class TCPListener:

    ## Initiate a TCP listener.
    # @param loop asyncio loop to operate on
    # @param host hostname to bind on
    # @param port port to bind on
    def __init__(self, loop, host, port):
        self.socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        self.socket.bind((host, port))

        self.loop = loop

    ## Asyncio coroutine that listens on self.port and spawns session.DispatcherSession to handle incoming requests
    # without blocking future requests.
    async def listen(self):

        self.socket.setblocking(False)
        self.socket.listen(20)

        while True:
            connection, _ = await self.loop.sock_accept(self.socket)
            connection.setblocking(True)

            dispatcher = lorawan_socket_handle.LoRaWANDispatcherSession(self.loop,connection,device_manager,send_packet)
            asyncio.ensure_future(dispatcher.dispatch_message(send_packet))

    ## Releases listener resources
    def teardown(self):
        self.socket.close()

## Run a single gateway.
if __name__ == "__main__":
   dispatcher = LoRaWAN_Dispatcher()
   dispatcher.run()

