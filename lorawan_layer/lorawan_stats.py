from config import LoRaWAN_STATS_LOG_INTERVAL
from flip_layer.logger import logger
import asyncio

class LoRaWAN_Dispatcher_Stats:
    def __init__(self):
        self.version = 0
        self.counters = {}
        self.counters['join_messages_sent_to_flip_layer'] = 0
        self.counters['uplink_messages_sent_to_flip_layer']=0
        self.counters['stats_messages_sent_to_flip_layer']=0
        self.counters['join_acks_sent_to_lorawan_layer']=0
        self.counters['total_messages_from_flip_layer']=0
        self.counters['total_messages_to_flip_layer']=0
    def log(self):
        logger.log("Stats", "LoRaWAN layer stats at version " + str(self.version))
        for k in self.counters:
            logger.log("Stats", k + " " + str(self.counters[k]))

    async def logging_task(self):
        while True:
            self.log()
            await asyncio.sleep(LoRaWAN_STATS_LOG_INTERVAL)

stats = LoRaWAN_Dispatcher_Stats()
