import socket
import asyncio
from flip_layer.logger import logger,MsgColour
from flip_layer.session import Session
import base64
from lorawan_layer.lorawan_stats import stats
from lorawan_layer.lorawan_basic_util import GatewayIdentifiers
import config
gw_id = GatewayIdentifiers()

## @package lorawan_socket handle
# Contains LoRaWANDispatcherSession and FLIPClientSession to request and answer services between layers.
##

ed_data = {
    "owned_end_devices": ["dev" ],
    # "owned_end_devices": ["devEUI1", "devEUI2"],
    "handled_end_devices": [],
    # "handled_end_devices": ["devEUI2", "devEUI3"],
    "heard_end_devices": {
        "dev": {
            "rssi": ( -128),
            "channel": 1,
            "sf": 7
        },
        "dev" : {
            "rssi": (-128),
            "channel": 2,
            "sf": 8
        },
        "dev": {
            "rssi": ( -128),
            "channel": 3,
            "sf": 9
        }
    }
}


##Handle Request from FLIP layer
#
class LoRaWANDispatcherSession(Session):
    def __init__(self,loop,connection,device_manager,send_packet):
        self.loop = loop
        self.connection = connection
        super().__init__(loop,connection)
        self.device_manager = device_manager

    ## Asyncio routine that handles an incoming service request from the FLIP  layer, possibly returns data over
    # self.connection and ends the session when done.
    async def dispatch_message(self,send_packet):

        try:
            parsed_msg = await self.listen()
        except Exception:
            logger.error("Dispatcher", "No good answer")
            return

        try:
            assert isinstance(parsed_msg["event"], str)
        except Exception as err:
            logger.error("LoRaWAN Socket Handle (Dispatcher Session)", "Message does not contain event field")
            self.connection.close()
            return

        logger.log("LoRaWAN Socket Handle (Dispatcher Session)", "Received message for event: " + parsed_msg["event"], MsgColour.LORAWAN_EVENT)
        stats.counters['total_messages_from_flip_layer']+=1
        # handler receives device data from FLIP layer
        if parsed_msg["event"] == "device_data_from_handler":#used to called "app_payload_from_handler"
            dev_eui = parsed_msg["ed_id"]
            try:
                appmsg = base64.b64decode(parsed_msg["lorawan_data"][0])
            except:
                logger.error("LoRaWAN Socket Handle (Dispatcher Session) - (device_data_from_handler)", "Message couldn't get encoded (Encoding Error)")
            logger.log("LoRaWAN Socket Handle (Dispatcher Session) - (device_data_from_handler)", "A device data (app payload) received from FLIP layer", MsgColour.LORAWAN_EVENT)
            device_type,dev_obj = self.device_manager.check_device_deveui(dev_eui)
            dev_obj.seq_nbr = parsed_msg["lorawan_data"][1]
            decrypted_payload = self.device_manager.decrypt_payload(dev_obj, appmsg)
            self.device_manager.save_payload(dev_obj, decrypted_payload)
            #get app_payload
            #have already nwkskey and appskey for the device
            #decrypt it and save it
            pass
        # FLIP layer provides networking data (keys etc) to be passed to new device handler
        elif parsed_msg["event"] == "install_handler":#used to called "handle_device"
            logger.log("LoRaWAN Socket Handle(Dispatcher Session)-(install_handler)", "Device keys and info (join_ack,nwkskey,devaddr) received from FLIP layer", MsgColour.LORAWAN_EVENT)
            dev_eui = parsed_msg["ed_id"]
            join_ack = parsed_msg['lorawan_data'][0]
            nwkskey = parsed_msg['lorawan_data'][1]
            devaddr = parsed_msg['lorawan_data'][2]
            dev_type,dev_obj = self.device_manager.check_device_deveui(dev_eui)
            if dev_obj == None:
                logger.error("LoRaWAN Socket Handle (Dispatcher Session) - (install_handler)", "The Device Obj does not exist")
                return;
            dev_obj.handle = True
            dev_obj.nwkskey = nwkskey
            dev_obj.devaddr = devaddr
            try:
                enc_join_ack = base64.b64decode(join_ack)
            except:
                logger.error("LoRaWAN Socket Handle (Dispatcher Session) - (install_handler)", "Message couldn't get encoded (Encoding Error)")

            logger.log("LoRaWAN Socket Handle", "put info to the queue",colour_prepend = MsgColour.LORAWAN_EVENT)

            send_packet.put(enc_join_ack)
            send_packet.join()
            logger.log("LoRaWAN Socket Handle", "queue job is done",colour_prepend = MsgColour.LORAWAN_EVENT)

            stats.counters['join_acks_sent_to_lorawan_layer']+=1
            #send join ack
            pass

        # Receive get stats that asks LoRaWAN layer to pass a device overview, as specified in lorawan_spoof.py at a later point in time
        elif parsed_msg["event"] == "get_stats":
            logger.log("LoRaWAN Socket Handle (Dispatcher Session)-(get_stats)", "Get stats request received from FLIP layer", MsgColour.LORAWAN_EVENT)
            #it's pushed, take a look, update lists and dev info
            data = self.device_manager.get_stats()
            #json_data = json.dumps(data)
            gw_id.cluster_id = parsed_msg["cluster_id"]
            gw_id.gateway_id = parsed_msg["gateway_id"]
            s = await FLIPClientSession.obtain(self.loop)
            s.pass_stats(data)
            s.end()

            pass
        # FLIP layer informs new handler, asking it to invoke a new_handler_response event at some later point in time
        elif parsed_msg["event"] == "new_handler":#used to called "prepare_new_handler_info"
            dev_eui = parsed_msg["ed_id"]
            msg = parsed_msg["lorawan_data"][0]
            dev_nonce = parsed_msg["lorawan_data"][1]
            mic = parsed_msg["lorawan_data"][2]
            channel = parsed_msg["channel"]
            msg_type = 'join_req'
            logger.log("LoRaWAN Socket Handle (Dispatcher Session) - (new_handler)", "A request for new device keys and info received from FLIP layer", MsgColour.LORAWAN_EVENT)
            device_type, dev_obj = self.device_manager.check_device_deveui(dev_eui)
            if device_type == 'unknown_device':
                dev_obj = None
                mic_check = None
            else:
                dev_obj.mic = mic
                dev_obj.handle = False
                dev_obj.dev_nonce = dev_nonce
                mic_check = self.device_manager.check_mic_joinreq(dev_obj, msg, "owned")

                if not mic_check:
                    dev_obj.dev_nonce = None
                    logger.log("LoRaWAN Socket Handle (Dispatcher Session) - (new_handler)",
                               "A MIC check is failed on join request",
                               MsgColour.LORAWAN_EVENT)
                else:
                    dev_nonce_check = self.device_manager.check_dev_nonce(dev_obj, dev_nonce)
                    if dev_nonce_check:
                        dev_obj.old_dev_nonce.append(dev_nonce)
                        join_ack = self.device_manager.create_join_ack(dev_obj, channel)
                        lorawan_message = []
                        try:
                            lorawan_message.append(str(base64.encodebytes(join_ack),'utf-8'))
                        except:
                            logger.error("LoRaWAN Socket Handle(Dispatcher Session)-(new_handler)", "Message couldn't get decoded(Decoding Error)")

                        lorawan_message.append(dev_obj.nwkskey)
                        lorawan_message.append(dev_obj.devaddr)
                        json_new_handler = {
                            "ed_id": dev_eui,
                            "lorawan_data": lorawan_message,

                        }
                        s = await FLIPClientSession.obtain(self.loop)
                        s.new_handler_response(json_new_handler)
                        s.end()
                    else:
                        dev_obj.dev_nonce = None
                        logger.log("LoRaWAN Socket Handle (Dispatcher Session) - (new_handler)",
                                   "Replay join request is detected, nothing pushed to FLIP layer",
                                   MsgColour.LORAWAN_EVENT)
            #logger.log("LoRaWAN Socket Handle(Dispatcher Session)-(new_handler)", "New device keys sent to FLIP Layer", MsgColour.LORAWAN_EVENT)
            #create keys
            #create join acks
            #create a session called joinackforhandler
            pass
        elif parsed_msg["event"] == "wait_forever":  # For testing purposes
            self.talk({"response": "Going to sleep!"})
            await asyncio.sleep(3600)
        elif parsed_msg["event"] == "call_me":  # For testing
            self.talk({"reply": "Calling FLIP layer for testing, this is not what the real flow should look like"})
            s = await FLIPClientSession.obtain(asyncio.get_event_loop())
            s.talk({"event": "Reached FLIP layer"})

        else:
            self.talk({"error": "Unknown event!"})
            logger.error("LoRaWAN Socket Handle (Dispatcher Session)", "Unknown event: " + parsed_msg["event"])

        self.end()

##Communicate with FLIP layer to request some service
# Do not use constructor directly, obtain a session through FLIPClientSession.obtain()
class FLIPClientSession(Session):
    def __init__(self, loop):
        super().__init__(loop, None)
    ## Asyncio coroutine that returns a usable session when connection with the lower layer has successfully
    # been established.
    # @returns A FLIPClientSession providing service from  the FLIP layer
    @staticmethod
    async def obtain(loop):
        s = FLIPClientSession(loop)
        await s.initiate()
        return s

    async def initiate(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setblocking(True)

        while True:
            try:
                sock.connect((config.HOST, config.FLIP_LOW_SERVER_PORT))
                break
            except ConnectionRefusedError as err:
                logger.error("FLIP client", "Cannot reach FLIP layer")
                await asyncio.sleep(3, self.loop)

        self.connection = sock

    ##Send join request to FLIP layer. (Possible handler receives join request from LoRaWAN layer)
    #@param msg join req message
    def join_request(self,msg):

        data = {
            "event": "join_request",#used to called "join_req_for_hand;er"
        }
        data.update(msg)
        self.talk(data)
        self.end()
        stats.counters['total_messages_to_flip_layer']+=1


    ##Provides application data to be passed to device owner
    # @param msg application payload
    def device_data_for_owner(self,msg):  # Will take more arguments, just an example for now
        data = {
            "event": "device_data_for_owner",#used to called "app_payload_for_owner"
        }
        data.update(msg)
        self.talk(data)
        self.end()
        stats.counters['total_messages_to_flip_layer']+=1
        logger.log("LoRaWAN Socket Handle (FLIPClientSession) - (device_data_for_owner)", "Device data provided to FLIP layer", MsgColour.LORAWAN_EVENT)

    ## Provides networking data (keys etc) to be passed to new device handler
    # @param networking data (keys etc)
    def new_handler_response(self,msg):  # Will take more arguments, just an example for now
        data = {
            "event": "new_handler_response",#used to called "new_handler_info"
        }
        data.update(msg)
        self.talk(data)
        self.end()
        stats.counters['total_messages_to_flip_layer']+=1
        logger.log("LoRaWAN Socket Handle (FLIPClientSession) - (new_handler_response)", "New handler response and keys provided to FLIP layer", MsgColour.LORAWAN_EVENT)


    ##Sends device overview to FLIP layer
    # @param device list
    def pass_stats(self,msg):
        data = {
            "event": "pass_stats", #
        }
        data.update(msg)
        self.talk(data)
        self.end()
        stats.counters['stats_messages_sent_to_flip_layer'] += 1
        stats.counters['total_messages_to_flip_layer']+=1
        logger.log("LoRaWAN Socket Handle (FLIPClientSession) - (pass_stats)", "Stats passed to FLIP layer", MsgColour.LORAWAN_EVENT)


    async def get_slow(self):  # Testing purposes, example only
        await asyncio.sleep(20)
        self.talk({"event": "being slow"})
        self.end()
