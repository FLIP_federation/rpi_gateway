import datetime
import os

class Logger():
    def __init__(self):
        self.gw_name = "GT_xx_xx"
        try:
            self.gw_id = os.environ["GW_ID"]
            self.gw_cluster = os.environ["GW_CLUSTER"]
            self.gw_name= "GT_" + self.gw_cluster + "_" + self.gw_id
        except Exception:
            pass

    def log(self, tag, message):
        now = datetime.datetime.now()
        if True:
            print("[" + now.strftime("%H:%M:%S - ") + self.gw_name + ": "+tag+"]", message)

    def error(self, tag, message):
        now = datetime.datetime.now()
        print("[" + now.strftime("%H:%M:%S - ") + self.gw_name + ": "+"ERROR: "+tag+"]", message)


logger = Logger()
