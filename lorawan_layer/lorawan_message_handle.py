import struct
from Cryptodome.Cipher import AES
from lorawan_layer.lorawan_crypto import aesEncrypt,aesDecrypt
from lorawan_layer.lorawan_basic_util import intPackBytes,intUnpackBytes
import codecs
from flip_layer.logger import logger
##@package lorawan_message_handle
# Contains message object for different type of LoRaWAN messages
# Provided by FloraNet implementation(https://github.com/Fluent-networks/floranet/blob/master/floranet/lora/mac.py)
# Major changes has been done to make it compatible with python3.6 and to fit into LoRaWAN layer FLIP architecture
#

##MAC Message Types
JOIN_REQUEST = 0
JOIN_ACCEPT = 1
UN_DATA_UP = 2
UN_DATA_DOWN = 3
CO_DATA_UP = 4
CO_DATA_DOWN = 5
PROPRIETARY = 7

##MAC Commands
LINKCHECKREQ = 2
LINKCHECKANS = 2
LINKADRREQ = 3
LINKADRANS = 3
DUTYCYCLEREQ = 4
DUTYCYCLEANS = 4
RXPARAMSETUPREQ = 5
RXPARAMSETUPANS = 5
DEVSTATUSREQ = 6
DEVSTATUSANS = 6
NEWCHANNELREQ = 7
NEWCHANNELANS = 7
RXTIMINGSETUPREQ = 8
RXTIMINGSETUPANS = 8

##The AppNonce is a random value or some form of unique ID provided by the network server
# and used by the end-device to derive the two session keys NwkSKey and AppSKey.
# constant values for join ack
app_nonce = 0xE5063A
netid = 0x000030
DLsettings = 0x03
RXdelay = 0x01
cflist = [0x28768400000000000000000000000000,0xF87D8400000000000000000000000000,0xC8858400000000000000000000000000,0x184f8400000000000000000000000000,0x000000e8568400000000000000000000,0x000000000000b85e8400000000000000,0x00000000000000000088668400000000,0x000000000000000000000000586e8400]


##Major version of data message (Major bit field)
LORAWAN_R1 = 0

##LoRa Message MAC Header.
#The MAC header specifies the message type (mtype)
#and according to which major version (major) of the
#frame format of the LoRaWAN layer specification used
#for encoding.

#Attributes:
#    mtype (int): Message type.
#    major (int): Major version.

##
class MACHeader(object):
    ##MACHeader initialisation method.
    #  @param  mtype (int): Message type.
    #  @param  major (int): Major version.


    def __init__(self, mtype, major):

        self.mtype = mtype
        self.major = major

    @classmethod
    ## Create a MACHeader object from binary representation.
    #
    #
    # @param    data (str): UDP packet data.
    #
    # @returns:
    #     MACHeader object on success, None otherwise.
    #
    #
    def decode(cls, data):
        h = struct.unpack('B', data)[0]
        # Bits 7-5 define the message type
        mtype = (h & 224) >> 5
        # Bits 1-0 define the major version
        major = h & 3
        m = MACHeader(mtype, major)
        return m

    ## Create a binary representation of MACHeader object.
    #
    #   @returns One character of data.
    #
    def encode(self):

        b = 0 | self.mtype << 5 | self.major
        data = struct.pack('B', b)
        return data


## Join Accept Message.
#
# The join accept message contains an
# application nonce of 3 octets (appnonce),
# 3 octet a network identifier (netid), a 4
# octet device address (devaddr), a 1 octet
# delay between tx and rx (rxdelay) and
# an optional list of channel frequencies
# (cflist).
#
#
# @param    mhdr (MACHeader): MAC header
# @param   appkey (int): Application key
# @param    appnonce (int): Application nonce
# @param    netid (int): Network identifer
# @param    devaddr (int): Device address
# @param    dlsettings (int): DLsettings field
# @param    rxdelay (int): Delay between tx and rx
# @param    cflist (list): List of channel frequencies
# @param    mic (int): Message integrity code
##
class JoinAcceptMessage():
    ##JoinAcceptMessage initialisation method.
    #
    #
    def __init__(self, appkey, appnonce, netid, devaddr, dlsettings,
                 rxdelay, cflist):

        self.mhdr = MACHeader(JOIN_ACCEPT, LORAWAN_R1)
        self.appkey = appkey
        self.appnonce = appnonce
        self.netid = netid
        self.devaddr = devaddr
        self.dlsettings = dlsettings
        self.rxdelay = rxdelay
        self.cflist = cflist
        self.mic = None

    ## Create a binary representation of JoinAcceptMessage object.
    #
    #        @returns: Packed JoinAccept message.
    #
    #
    #  Encoding Join-accept:
    #  MAC Header
    #  3 bytes appnonce
    #  3 bytes netid
    #  4 bytes devaddr
    #  1 byte dlsettings
    #  1 byte rxdelay
    #  Optional cflist
    #
    #  Create the message
    def encode(self):

        header = self.mhdr.encode()


        msg = intPackBytes(self.appnonce, 3, endian='little') + \
              intPackBytes(self.netid, 3, endian='little') + \
              struct.pack('<L', self.devaddr) + \
              struct.pack('B', self.dlsettings) + \
              struct.pack('B', self.rxdelay) + \
              intPackBytes(self.cflist, 16, endian='big')

        # CFList is not used in a Join Accept message for US/AU bands
        if self.cflist:
            pass
        # Create the MIC over the entire message
        self.mic = aesEncrypt(self.appkey, header  + msg,
                              mode='CMAC')[0:4]
        msg += self.mic
        # Add the header and encrypt the message using AES-128 decrypt
        data = header + aesDecrypt(self.appkey, msg)

        return data

    ##Create a NwkSKey or AppSKey
    #
    # Creates the session keys NwkSKey and AppSKey specific for
    # an end-device to encrypt and verify network communication
    # and application data.
    #
    #
    # @param    pre (int): 0x01 ofr NwkSKey, 0x02 for AppSKey
    # @param    app (Application): The applicaiton object.
    # @param    msg (JoinRequestMessage): The MAC Join Request message.
    #
    # @returns: int: 128 bit session key
    #

def createSessionKey(pre, appnonce, devnonce,netid,appkey):

    # Session key data: 0x0n | appnonce | netid | devnonce | pad (16)
    data = struct.pack('B', pre) + \
           intPackBytes(appnonce, 3, endian='little') + \
           intPackBytes(netid, 3, endian='little') + \
           struct.pack('<H', devnonce) + intPackBytes(0, 7)
    aesdata = aesEncrypt(appkey, data)
    key = intUnpackBytes(aesdata)
    return key


##Constructs join accept message
# @param devnonce received device nonce to construct join message
# @param max_dev_addr latest dev addr to create a new device address(this will be changed and going to be unique to FLIP)
# @channel channel selected channel freq for the end_device to communicate(send it to the end-device during joining procedure
#returns data encrypted join_ack message, nwkskey and appskey for join request and new dev addr



def join_accept_new(devnonce,new_dev_addr,channel,dev_appkey):
    #global offset
    #offset = offset + 1
    #new_devaddr = int(max_dev_addr,16) + offset


    response = JoinAcceptMessage(dev_appkey, app_nonce,
                                 netid, new_dev_addr,
                                 DLsettings, RXdelay,cflist[channel])
    data = response.encode()
    nwkskey = createSessionKey(1,app_nonce,devnonce,netid,dev_appkey)
    appskey = createSessionKey(2,app_nonce,devnonce,netid,dev_appkey)

    return data,nwkskey,appskey
DIR=0 # UPLINK

## Decrypts application payload(this will be removed and AES encrypt lib will be used for this purpose)
# @param appskey application session key to decrypt the payload
# @param dev_addr device address for corresponds to particular message
# @seq_nbr sequence number of the message used in decryption
# @appms application payload to be decrypted.
# @returns decrypted payload
def decrypt_appmsg(appskey,dev_addr, seq_nbr, appmsg):
    len_appmsg = len(appmsg)
    msg_pad = bytearray(appmsg)
    if len_appmsg < 16:
        i = len(appmsg)
        while i < 16:
            msg_pad.append(0x0)
            i+=1

    aBlock = bytearray("\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", 'utf-8')

    formatted_devaddr = int(dev_addr,16)


    aBlock[5] = int(DIR);

    aBlock[6] = (formatted_devaddr) & 0xFF;
    aBlock[7] = (formatted_devaddr >> 8) & 0xFF;
    aBlock[8] = (formatted_devaddr >> 16) & 0xFF;
    aBlock[9] = (formatted_devaddr >> 24) & 0xFF;

    aBlock[10] = (seq_nbr) & 0xFF;
    aBlock[11] = (seq_nbr >> 8) & 0xFF;
    aBlock[12] = (seq_nbr >> 16) & 0xFF;
    aBlock[13] = (seq_nbr >> 24) & 0xFF;


    aBlock[15] = 1;


    IV = bytes(chr(0) * 16,'utf-8')
    mode = AES.MODE_CBC

    #try:
    decryptor = AES.new(intPackBytes(appskey,16),mode,IV=IV )
    #except:
        # try:
        #     decryptor = AES.new(codecs.decode(appskey, 'hex'), mode, IV=IV)
        # except:
        #     logger.log("Message handling", "Received message couldn't decrypted")

    sBlock = decryptor.encrypt(aBlock)
    sBlock_array = bytearray((sBlock))


    i = 0;
    unencryptedmsg = bytearray()
    while i < len_appmsg:
        unencryptedmsg.append(msg_pad[i] ^sBlock_array[i])
        i +=1
    return unencryptedmsg


