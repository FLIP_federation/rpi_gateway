## @package config_lorawan_layer
#
# Low level parser config
# Dont change them unless you know what you're doing
# @param UDP_IP localhost parameter to connect driver layer
# @param UDP_PORT port parameter to receive messages from driver layer
# @param UDP_PORT_DOWN port parameter to send messages to driver layer
# @param ACK_JSON json packet format for downlink messages to end-device


UDP_IP = "127.0.0.1"
UDP_PORT = 1680
UDP_PORT_DOWN = 1681

## The port at which the LoRaWAN layer provides service
LORAWAN_SERVER_PORT = 9400

## The port at which the FLIP layer provides service to the LoRaWAN layer
FLIP_LOW_SERVER_PORT = 9401

ACK_JSON = '{"txpk":{"imme":false, "tmst":145244572, "freq":869.525000,"rfch":0,"powe":14,"modu":"LORA","datr":"SF9BW125","codr":"4/5","ipol":true,"size":33,"data":"01","ncrc":false}}'

RSSI_OFFSET = 0
