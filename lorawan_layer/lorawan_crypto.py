from Cryptodome.Cipher import AES
from Cryptodome.Hash import CMAC


## @package crypto
# Contains encryption/decryption functionality used in LoRaWAN specs.
## .


## Provides AES encryption functionality in two different modes(CMAC and ECB)
# @param key encryption key
# @param data to encrypt
# @mode to select encryption mode
# @returns encrpyted data
def aesEncrypt(key, data, mode=None):
    """AES encryption function

    Args:
        key (str): packed 128 bit key
        data (str): packed plain text data
        mode (str): Optional mode specification (CMAC)

    Returns:
        Packed encrypted data string
    """
    if mode == 'CMAC':
        # Create AES cipher using key argument, and encrypt data
        cipher = CMAC.new(key, ciphermod=AES)
        cipher.update(data)
        return cipher.digest()

    elif mode == None:
        cipher = AES.new(key, AES.MODE_ECB)
        return cipher.encrypt(data)

## Provides AES decryption functionality(only in ECB mode)
# @param key decryption key
# @param data to decrypt
# @returns decrpyted data
def aesDecrypt(key, data):
    """AES decryption fucnction

    Args:
        key (str): packed 128 bit key
        data (str): packed encrypted data

    Returns:
        Packed decrypted data string
    """
    cipher = AES.new(key, AES.MODE_ECB)
    return cipher.decrypt(data)

