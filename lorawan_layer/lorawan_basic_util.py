import codecs
import struct
import logging
from flip_layer.logger import logger


## @package lorawan_basic_util
# Basic utilization funcitonality coming from FloraNet used in lorawan_message handle package
##


## Convert an integer to a packed binary string representation.
#
#
#     @param    n (int): Integer to convert
#     @param   length (int): converted string length
#     @param   endian (str): endian type: 'big' or 'little'
#
#     @returns: A packed binary string.
#
def intPackBytes(n, length, endian='big'):

    h = '%x' % n
    s = codecs.decode(('0' * (len(h) % 2) + h).zfill(length * 2), 'hex')
    if endian == 'big':
        return s
    else:
        return s[::-1]


##Convert an packed binary string representation to an integer.
#
#
# @param    data (str): packed binary data
# @param   endian (str): endian type: 'big' or 'little'
#
# @returns: An integer.
#
def intUnpackBytes(data, endian='big'):

    if isinstance(data, str):
        data = bytearray(data)
    if endian == 'big':
        data = reversed(data)
    num = 0
    for offset, byte in enumerate(data):
        num += byte << (offset * 8)
    return num

##Converts decrpyted payload into strings and prints it
# @param msg decrypted payload(bytes)
# @returns string decrypted payload
def msg2string(msg):

    msg_str = ""

    try:
        logger.log ("Enc/Dec Module","Decrypted Frame Payload: ")
        for i in msg:
           print ("%02x" % (i),)

    except struct.error:
        logger.error ("Enc/Dec Module","ERROR unpacking message")
        logger.error("Enc/Dec Module","Parsing msg")
        msg_str=""
        for i in msg:
            msg_str+= "%02x " % (ord(i))
        logger.log("Enc/Dec Module",msg_str)

    return msg_str

class GatewayIdentifiers:
    def __init__(self):
        self.cluster_id = 0
        self.gateway_id = 0