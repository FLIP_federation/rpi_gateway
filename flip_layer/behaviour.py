import asyncio
import config
from math import sqrt
from time import time
from random import randint
from flip_layer.logger import logger
from flip_layer.gateway_stub import GatewayStub
from flip_layer.session import LoRaWANClientSession
from datetime import datetime, timedelta
import random
import aiohttp
import string
import subprocess
import re
import queue


## Class encapsulating a gateway's KNN/RPS maintenance behaviour.
class KNN:
    def __init__(self, gw):

        # gateway.Gateway running this task
        self.gw = gw

        # k in kNN
        self.K = config.K

        # Dictionary (identifier [string] -> gateway_stub.GatewayStub) containing all other gateways a gateway is aware of.
        # All gateways in self.knn, self.rps and self.bootstrap should be represented in this dict.
        self.view = {}

        # Set of string identifiers identifying the k most similar instances of gateway_stub.GatewayStub a gateway is aware of
        self.knn = set([])

        # Set of string identifiers identifying the instances of gateway_stub.GatewayStub currently used for random peer sampling
        self.rps = set([])

        # Seed kNN and RPS set
        self.bootstrap()

        self.sleep_task = None

        # Used to figure out whether knn rounds have converged
        self.last_knn = None
        self.knn_similarity_counter = 0

    ## Seed kNN and RPS set.
    # Currently selects a random subset of gateway_stub.GatewayStub instances supplied in config.py
    def bootstrap(self):
        # TODO
        choices_knn = set([])
        choices_rps = set([])

        if len(self.knn) == 0:
            for i in range(0, min(self.K, len(config.bootstrapping_gateways))):
                choices_knn.add(random.choice(range(0,len(config.bootstrapping_gateways))))
        if len(self.rps) == 0:
            for i in range(0, min(self.K, len(config.bootstrapping_gateways))):
                choices_rps.add(random.choice(range(0,len(config.bootstrapping_gateways))))

        for k in choices_knn:
            config.bootstrapping_gateways[k].maintaing_gw = self
            self.view[config.bootstrapping_gateways[k].get_identifier()] =  config.bootstrapping_gateways[k]
            self.knn.add(config.bootstrapping_gateways[k].get_identifier())

        for  r in choices_rps:
            config.bootstrapping_gateways[r].maintaing_gw = self
            self.view[config.bootstrapping_gateways[r].get_identifier()] =  config.bootstrapping_gateways[r]
            self.rps.add(config.bootstrapping_gateways[r].get_identifier())

    ## Coroutine that increments the age of all stubs in view.
    async def age_stubs(self):
        while True:
            await asyncio.sleep(config.AGING_INTERVAL)
            for id in self.view:
                self.view[id].increase_age()

    # Update stub in view with info if more recent.
    # @return GatewayStub based on most recent info
    def update_view_with_info(self, info):
        s = GatewayStub.create_from_info(self.gw, info)
        if s.get_identifier() == self.gw.get_identifier():
            return self.gw
        elif s.get_identifier() in self.view:
            if s.age <= self.view[s.get_identifier()].age:
                self.view[s.get_identifier()] = s
                return s
            else:
                return self.view[s.get_identifier()]
        else:
            self.view[s.get_identifier()] = s
            return s

    ## Interrupt this KNN instance's sleep after convergence if config.INTERRUPT_KNN_CONVERGENCE is set.
    def interrupt_convergence(self):
        if not (self.sleep_task is None):
            logger.log("KNN routine", "Interrupting KNN sleep because of diff between " + str(self.last_knn) + " and " + str(self.knn))
            self.sleep_task.cancel()

    ## Coroutine to be scheduled as a background task to handle KNN/RPS behaviour.
    # Runs knn/rps rounds until convergence, then sleeps, then repeats.
    async def run(self):
        while True:
            logger.log("KNN routine", "Woke up for new kNN cycle")
            self.sleep_task = None
            await self.knn_cycle()

            self.knn_similarity_counter = 0

            sleep_coro = asyncio.sleep(config.KNN_CYCLE)
            self.sleep_task = asyncio.ensure_future(sleep_coro)
            try:
                await self.sleep_task
            except asyncio.CancelledError:
                logger.log("KNN cycle", "Woke up from convergence sleep")
                continue

    ## Run knn/rps rounds till convergence.
    async def knn_cycle(self):

        self.gw.stats.counters["knn_cycles_started"] += 1
        while True:
            await asyncio.sleep(config.KNN_REFRESH_RATE*(1+random.random()))

            logger.log("KNN routine", "View: " + str([k + " age " + str(self.view[k].age) for k in self.view.keys()]))
            logger.log("KNN routine", "RPS: " + str(self.rps))
            logger.log("KNN routine", "kNN: " + str(self.knn) + " (conv count: " + str(self.knn_similarity_counter) + ")")

            # Order the rps stubs from young to old
            sorting_list = []
            now = time()
            for id in self.rps:
                sorting_list.append((id, now - self.view[id].rps_timestamp, now - self.view[id].created_timestamp, self.view[id]))

            sorted_list = sorted(sorting_list,
                                 key=lambda scoring: (scoring[1], -scoring[2], randint(0, 999)),
                                 reverse=False)

            sorted_stubs = [item[3] for item in sorted_list]
            logger.log("KNN routine", "SORTED_LIST: "+ str([str(elem[3]) + " : " + str(elem[1]) for elem in sorted_list]))

            # Update oldest by pushing lower half of our rps view
            try:
                oldest = sorted_stubs[-1] # Retrieve stub
            except Exception as exc:
                logger.error("KNN routine", "No one to push RPS view to")
                self.bootstrap() # Refill rps
                continue
            lower_half = sorted_stubs[-int((self.K / 2)+1):-1]

            try:
                await oldest.rps_update(self.gw.get_info(), {x.get_identifier(): x.get_info() for x in lower_half})
                logger.log("KNN routine", "Pushing RPS to " + str(oldest.get_identifier()))
                oldest.touch()
            except Exception as exc:
                logger.warn("KNN routine", "Error in RPS update push: " + str(exc))
                self.remove_unreachable(oldest.get_identifier())

            # Update our rps info
            for id in self.rps.copy():
                try:
                    info = await self.view[id].retrieve_info()
                    self.update_view_with_info(info)
                except Exception as exc:
                    logger.warn("KNN routine", "Could not update info of gateway stub in RPS view: " + str(exc))
                    self.remove_unreachable(id)

            # Pull from our k nearest neighbours
            for id in self.knn.copy():
                try:
                    info, knn_info_dict = await self.view[id].knn_update(self.gw.get_info())
                    s = self.update_view_with_info(info)

                    for id in knn_info_dict:
                        self.update_view_with_info(knn_info_dict[id])
                except Exception as exc:
                    logger.warn("KNN routine", "Could not pull from kNN neighbour: " + str(exc))
                    self.remove_unreachable(id)

            converged = self.adjust_knn()

            # Clean
            self.view = {s: self.view[s] for s in self.view
                                if s in (self.knn | self.rps)}
            # RPS set size is enforced in REST API handler

            if converged:
                self.gw.stats.counters["knn_cycles_converged"] += 1
                break

            # In case we have lost track of all possible connections with the federation
            if len(self.knn) == 0 or len(self.rps) == 0:
                logger.log("KNN routine", "Bootstrap again")
                self.bootstrap()

    ## Use view to calculate and set knn set.
    ## Use view to calculate and set knn set.
    # @param increase_similarity_counter True if the call to adjust_knn() should be considered as a possible step towards convergence.
    # @return True if knn set has converged
    def adjust_knn(self):
        neighbour_scores = []
        for id in self.view:
            stub = self.view[id]
            other_profile = stub.get_profile()
            score = self.calculate_similarity(other_profile)
            neighbour_scores.append((score, stub))

        sorting_list = sorted(neighbour_scores, key = lambda scoring: (scoring[0], str(scoring[1])), reverse = True)

        self.knn = set([elem[1].get_identifier() for elem in sorting_list[:self.K]])

        if self.last_knn == self.knn:
                self.knn_similarity_counter += 1
        else:
            self.knn_similarity_counter = 0
        self.last_knn = self.knn.copy()

        if self.knn_similarity_counter >= config.KNN_CONVERGENCE_THRESHOLD:
            self.knn_similarity_counter = config.KNN_CONVERGENCE_THRESHOLD
            logger.log("KNN routine", "Converged on kNN " + str(self.knn))
            return True
        return False

    ## Purge references to a gateway_stub.GatewayStub because it is unreachable.
    # @param identifier string identifier of the stub to be purged
    def remove_unreachable(self, identifier):
        self.rps.discard(identifier)
        self.knn.discard(identifier)
        del self.view[identifier]

    # Trim rps set to size.
    def prune_rps_set(self):
        # Randomly select subset and ditch the rest such that K remain
        to_del = len(self.gw.knn_task.rps) - self.gw.knn_task.K
        for i in range(to_del):
            chosen = random.choice(tuple(self.gw.knn_task.rps))
            self.gw.knn_task.rps.remove(chosen)

    ## Calculate similarity of running gateway to another gateway as described by its kNN profile
    # @param profile other gateway's profile, e.g. {"dev1": 5, "dev2", 10}
    def calculate_similarity(self, profile):
        # TODO range of rssi values?
        my_profile = {k: v - config.MIN_RSSI for k,v in self.gw.get_profile().items()}
        other_profile = {k: v - config.MIN_RSSI for k,v in profile.items()}

        # fill out both dicts
        for ed in my_profile:
            if ed not in other_profile:
                other_profile[ed] = 0

        for ed in other_profile:
            if ed not in my_profile:
                my_profile[ed] = 0

        # numerator of the cosine similarity function
        sum_a_x_b = 0
        for ed in my_profile:
            sum_a_x_b += my_profile[ed] * other_profile[ed]

        # denominator of the cosine similarity function
        sum_a_2 = 0
        sum_b_2 = 0
        for ed1, ed2 in zip(my_profile, other_profile):
            sum_a_2 += my_profile[ed1] ** 2
            sum_b_2 += other_profile[ed2] ** 2

        denom = sqrt(sum_a_2) * sqrt(sum_b_2)

        # result
        try:
            res = sum_a_x_b / denom
        except ZeroDivisionError:
            res = 0
        return res



## Class encapsulating background task to periodically poll LoRaWAN layer for info
class LoRaWANPoll:
    def __init__(self, gw):
        self.gw = gw

    ## Coroutine to be scheduled as a background task to periodically poll LoRaWAN layer.
    async def run(self):
        while True:
            s = await LoRaWANClientSession.obtain(self.gw.loop, self.gw)
            logger.log("LoRaWAN poll", "Polling for gateway stats")
            s.get_stats()
            await asyncio.sleep(config.LORAWAN_POLL_CYCLE)

## Class encapsulating background behaviour tasks relating to end device subscriptions.
class EDMaintenance:
    def __init__(self, gw):
        self.gw = gw

        ## A dictionary (end device id (string) -> handler info) detailing end devices owned by the running gateway.
        # The format used to represent handler info should be consistent with get_info() in gateway_stub.GatewayStub.
        self.handler_for_owned_eds = {}

        ## A dictionary (end device id (string) -> subscription) containing the subscriptions that the running gateway is aware of.
        # The format of a subscription is detailed in create_sub() bellow.
        self.subs = {}

        ## A dictionary (message id (string) -> typeof(datetime.now()) detailing when a message with a given id was received.
        # Purely for dissemination purposes.
        self.sub_messages = {}

        # dict ed_id -> join attempt
        self.join_attempts = {}

        # dict ed_id -> message
        self.consensus_messages = {}

        # dict ed_id -> join message
        self.ed_handler_candidates = {}

        # dict ed_id -> stub info
        self.ed_handlers = {}

        # dict ed_id -> list<message_id>
        self.held_subscriptions = {}

    ## Coroutine that periodically spreads subscriptions for owned end devices.
    async def spread_subs(self):
        while True:
            if not (self.gw.ed_data is None):
                for ed_id in self.gw.ed_data["owned_end_devices"]:
                    await asyncio.sleep(1)
                    sub = self.create_sub(ed_id)
                    if not (ed_id in self.held_subscriptions):
                        self.held_subscriptions[ed_id] = []
                    self.held_subscriptions[ed_id].append(sub["message_id"])
                    if len(self.held_subscriptions[ed_id]) > config.NUM_HELD_SUBS: # Keep 5 most recent ones for a given ed_id
                        del self.held_subscriptions[ed_id][0]
                    self.gw.stats.counters["sub_messages_sent_for_owned_device"] += 1
                    await self.gw.local_sub(sub)
                    try:
                        if self.gw.my_leader is None:
                            logger.warn("Spreading subs", "Warning: leader is None")
                            continue
                        logger.log("Spreading subs", "Sending sub " + sub["message_id"] + " to my leader " + \
                                   self.gw.my_leader.get_identifier() + " for global dissemination")
                        await self.gw.my_leader.global_sub({
                            "sub": sub,
                            "tree": None #To be filled in by receiving end
                        })
                    except Exception as e:
                        logger.error("Spreading subs", "Error: could not reach local cluster leader: " + str(e))
                        self.gw.my_leader = None
            logger.log("ED maintenance", "Next Subscriptions Round will start in " + str(config.SUB_DELAY) + "s")
            await asyncio.sleep(config.SUB_DELAY)
            logger.log("ED maintenance", "Currently held subscriptions: " + str(self.held_subscriptions))


    ## Coroutine that cleans up expired subscriptions
    async def maintenance(self):
        while True:
            await asyncio.sleep(config.ED_MAINTENANCE_INTERVAL)

            prev_len = len(self.sub_messages)
            self.sub_messages = dict(filter(lambda x: (datetime.now() - x[1]).total_seconds() < config.MAX_SUB_MESSAGE_AGE, self.sub_messages.items()))
            now_len = len(self.sub_messages)
            logger.log("ED maintenance", "Cleaned messages from size " + str(prev_len) + " to size " + str(now_len))

            old_subs = self.subs
            logger.log("ED maintenance", "Old subs: " + str({k: old_subs[k]["expires"] for k in old_subs}))
            for id in self.subs.copy():
                self.subs[id]["expires"] -= 1
                if self.subs[id]["expires"] <= 0:
                    del self.subs[id]
            logger.log("ED maintenance", "Subs after maintenance: " + str({k: self.subs[k]["expires"] for k in self.subs}))


            self.ed_handlers = dict(filter(lambda x:
                                             x[0] in self.gw.ed_data["owned_end_devices"],
                                             self.ed_handlers.items()))

            self.held_subscriptions = dict(filter(lambda x:
                                             x[0] in self.gw.ed_data["owned_end_devices"],
                                             self.held_subscriptions.items()))

            self.join_attempts = dict(filter(lambda x:
                                             (datetime.now() - x[1]["timestamp"]).total_seconds() < config.CONSENSUS_MESSAGE_EXPIRY,
                                             self.join_attempts.items()))

            self.consensus_messages = dict(filter(lambda x:
                                 (datetime.now() - x[1]["timestamp"]).total_seconds() < config.CONSENSUS_MESSAGE_EXPIRY,
                                 self.consensus_messages.items()))

    ## Return a dictionary describing a subscription for a given end device.
    # @param ed_id The (globally unique) identifier (string) for that end device.
    # @returns Said dictionary.
    def create_sub(self, ed_id):
        info = self.gw.get_info()
        info_subset = {k: info[k] for k in info if k in ["host", "flip_high_server_port", "flip_high_server_leader_port", "cluster_name", "is_leader"]}
        return {
            "message_id": "".join([random.choice(string.ascii_letters + string.digits) for _ in range(16)]),
            "ed_id": ed_id,
            "owner_info": info_subset,
            "expires": int(config.SUB_DURATION/config.ED_MAINTENANCE_INTERVAL) # Number of ED maintenance ticks
        }

    ## Create message to be used in local consensus.
    # @param ed_id Globally unique identifier of the end device that is trying to join
    # @param best known occupation at which that device can be handled
    # @return the message
    def create_local_consensus_message_for_join(self, ed_id, occ):
        info = self.gw.get_info()
        info_subset = {k: info[k] for k in info if k in ["host", "flip_high_server_port", "flip_high_server_leader_port", "cluster_name", "is_leader"]}
        return {
            "message_id": "".join([random.choice(string.ascii_letters + string.digits) for _ in range(16)]),
            "ed_id": ed_id,
            "occupation": occ,
            "channel_occupation": {self.gw.get_identifier(): self.gw.calculate_channel_occupation()},
            "timestamp": None,
            "sender_info": info_subset # for traceability
        }


    # Remember join request to support local consensus protocol.
    # @param ed_id globally unqiue identifier of end device sending the join request
    # @param occ occupation at which running gateway can handle that device
    # @param lorawan_data LoRaWAN-layer data to be passed to owner in case running gateway wins local consensus
    def add_join_attempt(self, ed_id, occ, lorawan_data):
        self.gw.ed_maintenance.join_attempts[ed_id] = {"timestamp": datetime.now(),
                                                       "occupation": occ,
                                                       # Indicate whether we've already called LocalConsensus.do_won_consensus() for this join attempt
                                                       "did_win": False,
                                                       "lorawan_data": lorawan_data}

    ## Return message to be passed to end device owner to inform it that the running gateway is a candidate handler.
    # @param consensus_message message with which local consensus was won
    # @return the message
    def create_handler_message(self, consensus_message):
        info = self.gw.get_info()
        info_subset = {k: info[k] for k in info if k in ["host", "flip_high_server_port", "flip_high_server_leader_port", "cluster_name", "is_leader"]}
        return {
            "ed_id": consensus_message["ed_id"],
            "sub_message_id": self.subs[consensus_message["ed_id"]]["message_id"],
            "handler_info": info_subset,
            "channel": self.gw.calculate_best_channel(consensus_message["ed_id"], consensus_message["channel_occupation"]),
            "occupation": consensus_message["occupation"],
            "lorawan_data": self.join_attempts[consensus_message["ed_id"]]["lorawan_data"]
        }


    ## Return handler message to be passed to newly accepted handler's HandlerAccept API endpoint (passing keys, join ack ...).
    # @param ed_id Globally unique identifier for the originating end device
    # @param lorawan_data Data from handler LoRaWAN layer to be passed as-is to new handler's LoRaWAN layer
    # @param point in time (format: str(datetime.now())) where handler gateway should stop handling the device (currently not used by LoRaWAN layer?)
    # @return the message
    def create_handler_accept_message(self, ed_id, lorawan_data, expires):
        info = self.gw.get_info()
        info_subset = {k: info[k] for k in info if k in ["host", "flip_high_server_port", "flip_high_server_leader_port", "cluster_name", "is_leader"]}
        return {
            "ed_id": ed_id,
            "lorawan_data": lorawan_data,
            "owner_info": info_subset # for traceability
        }

    ## Return device data message to be passed to owner's DeviceData API endpoint
    # @param ed_id Globally unique identifier for the originating end device
    # @param lorawan_data Data from handler LoRaWAN layer to be passed as-is to owner's LoRaWAN layer
    # @return the message
    def create_device_data_message(self, ed_id, lorawan_data):
        info = self.gw.get_info()
        info_subset = {k: info[k] for k in info if k in ["host", "flip_high_server_port", "flip_high_server_leader_port", "cluster_name", "is_leader"]}
        return {
            "ed_id": ed_id,
            "lorawan_data": lorawan_data,
            "handler_info": info_subset # for traceability
        }

## Class encapsulating leader behaviour for global overlay construction
class ClusterInterconnect:
    def __init__(self, gw):


        self.gw = gw

        # dict from cluster name (string) to GatewayStub for corresponding leader
        self.leaders = {
            config.CLUSTER_NAME: gw
        }

        # dict from cluster name (string) to (dict of cluster_name (string) to distance (number))
        self.distances = {}

        # dict from cluster name (string) to list<string> detailing tree structure (next hop for every cluster)
        self.tree = {
            config.CLUSTER_NAME: []
        }

    ## Calculate tree of shortest path with Dijkstra's algorithm
    # @param graph dict <string -> dict <string -> number>> indicating distances between graph labels
    # @param starting label in graph
    # @return dict <string -> list<string>> indicating next hops for every node in tree
    def dijkstra_tree(self, graph, start):

        visited = set([])
        directions = set([])
        q = queue.PriorityQueue()

        # cost, node, previous
        q.put((0, start, None))

        while not q.empty():
            current_cost, current_node, current_prev = q.get()
            if current_node in visited:
                continue
            visited.add(current_node)
            directions.add((current_cost, current_node, current_prev))

            for neighbour in graph[current_node]:
                if not (neighbour in visited):
                    q.put((current_cost + graph[current_node][neighbour], neighbour, current_node))

        result = {}
        for d in directions:
            cost, node, previous = d
            if (not (previous is None)) and (not (previous in result)):
                result[previous] = []
            if (not (previous is None)):
                result[previous].append(node)

        return result


    ## Return time to ping a given ip address
    # @return time
    def ping(self, ip_str):
        output = subprocess.check_output("ping -c 1 "+ip_str, shell=True)
        result = float(re.search("mdev = .+/(.+)/.+/.+", str(output)).group(1))
        return result

    ## Background task which periodically builds a tree connecting leaders of different clusters.
    async def discover_leaders(self):
        while True:
            leaders = self.leaders
            distances = {self.gw.cluster_name: {}}

            for cluster_name in config.CLUSTER_DEFINITIONS:
                if cluster_name in leaders:
                    continue
                for ip in config.CLUSTER_DEFINITIONS[cluster_name]:
                    tentative_stub = GatewayStub(host = str(ip),
                                flip_high_server_port = str(9002), flip_high_server_leader_port = str(9003), cluster_name = cluster_name)
                    try:
                        response = await tentative_stub.leader_info(self.gw.get_identifier())
                        if not (response["leader_info"] is None):
                            remote_leader_stub = GatewayStub.create_from_info(self.gw, response["leader_info"])
                            leaders[cluster_name] = remote_leader_stub
                            logger.log("Cluster interconnect", "Learned that " + remote_leader_stub.get_identifier() + \
                                        " leads " + cluster_name + " from " + str(ip))
                            break
                    except aiohttp.client_exceptions.ClientConnectorError as e:
                        pass
                    await asyncio.sleep(0.1)

            to_del = set([])
            for leader_name in leaders:
                if leader_name == config.CLUSTER_NAME:
                    continue
                leader = leaders[leader_name]
                try:
                    response  = await leader.interconnect_info(self.gw.get_identifier())
                    pingtime = self.ping(leader.host)
                except aiohttp.client_exceptions.ClientConnectorError:
                    to_del.add(leader_name)
                distances[leader.cluster_name] = response["my_interconnect"]
                distances[self.gw.cluster_name][leader.cluster_name] = pingtime
                await asyncio.sleep(0.1)

            for name in to_del:
                del leaders[to_del]


            self.leaders = leaders
            self.distances = distances
            self.tree = self.dijkstra_tree(self.distances, self.gw.cluster_name)
            logger.log("Cluster interconnect", "Leaders: " + str({k: self.leaders[k].get_identifier() for k in self.leaders}))
            logger.log("Cluster interconnect", "Distances: " + str(self.distances))
            logger.log("Cluster interconnect", "Tree: " + str(self.tree))
            await asyncio.sleep(config.CLUSTER_INTERCONNECT_INTERVAL/2*(1+random.random()))


