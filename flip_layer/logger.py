import datetime
import os

class MsgColour:
    ERROR   = "\033[1;31m" 		# red bold
    WARNING  = "\033[1;33m"		# yellow bold
    INIT  = "\033[36m"			# normal cyan
    NOMINAL = "\033[0;32m"		# normal green
    EOM = "\033[0;0m"			# end of message sequence
    BOLD    = "\033[;1m"		# white bold
    LORAWAN_EVENT = "\033[1;35m" # Communication between LoRaWAN and FLIP layer to deal with some external event
    FLIP_EVENT = "\033[1;36m"    # Communication between two FLIP layers to deal with some external event

# Overrides
filter = set(["Exclude this tag", "and this one too"])
colours = {
    "a tag": "a colour",
}

class Logger():
    def __init__(self):
        self.gw_name = "GT_xx_xx"
        try:
            self.gw_id = os.environ["GW_ID"]
            self.gw_cluster = os.environ["GW_CLUSTER"]
            self.gw_name= "GT_" + self.gw_cluster + "_" + self.gw_id
        except Exception:
            pass

    def log(self, tag, message, colour_prepend = ""):
        if tag in colours:
            colour_prepend = colours[tag]

        if tag not in filter:
            self.print_in_colour(tag, message, colour_prepend)

    def warn(self, tag, message):
        self.print_in_colour(tag, message, MsgColour.WARNING)

    def error(self, tag, message):
        self.print_in_colour(tag, message, MsgColour.ERROR)

    def print_in_colour(self, tag, message, colour_prepend):
        now = datetime.datetime.now()
        print(colour_prepend + "[" + now.strftime("%Y-%m-%d %H:%M:%S - ") + self.gw_name + ": " + tag + "]", message + MsgColour.EOM)

logger = Logger()