import asyncio
import socket
from datetime import datetime

import flip_layer.session as session
from flip_layer.logger import logger
import os
import config


NUM_END_DEVICES = 10

config_version = 1
# ed_data = {
#     "owned_end_devices": ["dev" + str((config_version - 1) % NUM_END_DEVICES)],
#     # "owned_end_devices": ["devEUI1", "devEUI2"],
#     "handled_end_devices": handling,
#     # "handled_end_devices": ["devEUI2", "devEUI3"],
#     "heard_end_devices": {
#         "dev" + str((config_version - 1) % NUM_END_DEVICES): {
#             "rssi": (-50 if config_version == 1 else -128),
#             "channel": 1,
#             "sf": 7
#         },
#         "dev" + str(config_version): {
#             "rssi": (-50 if config_version == 2 else -128),
#             "channel": 2,
#             "sf": 8
#         },
#         "dev" + str((config_version + 1) % NUM_END_DEVICES): {
#             "rssi": (-50 if config_version == 0 else -128),
#             "channel": 3,
#             "sf": 9
#         }
#     }
#   # SAME THING FOR heard_end_devices_dev_addr
# }

handled = False
SIMULATED_DEVICE = "dev0"
ed_data = [
    {
        "deveui": "dev" + str((config_version - 1) % NUM_END_DEVICES),
        "dev_addr": "Adev" + str((config_version - 1) % NUM_END_DEVICES),
        "rssi": -128,
        "sf": "SF7BW125",
        "channel": 1,
        "handled": False,
        "owned": True
    },
    {
        "deveui": "dev" + str(config_version),
        "dev_addr": "Adev" + str(config_version),
        "rssi": -50,
        "sf": "SF8BW125",
        "channel": 2,
        "handled": False,
        "owned": False
    },
    {
        "deveui": "dev" + str((config_version + 1) % NUM_END_DEVICES),
        "dev_addr": "Adev" + str((config_version + 1) % NUM_END_DEVICES),
        "rssi": -128,
        "sf": "SF9BW125",
        "channel": 3,
        "handled": False,
        "owned": False
    }
]



os.environ["LOG_ID"] = "LoRaWAN spoof"

class LoRaWANDispatcherSession(session.Session):

    async def dispatch_message(self, loop):

        try:
            parsed_msg = await self.listen()
        except Exception:
            logger.error("LoRaWAN dispatcher", "No good answer")
            return

        try:
            assert isinstance(parsed_msg["event"], str)
        except Exception as err:
            logger.error("LoRaWAN dispatcher", "Message does not contain event field")
            self.connection.close()
            return

        logger.log("LoRaWAN dispatcher", "Received message for event: " + parsed_msg["event"])

        if parsed_msg["event"] == "get_stats":
            s = await FLIPClientSession.obtain(loop)
            s.talk({
                "event": "pass_stats",
                "heard_end_devices": ed_data,
            })
            s.end()

        elif parsed_msg["event"] == "new_handler":
            logger.log("LoRaWAN dispatcher", "Received request for handler info for device " +
                       str(parsed_msg["ed_id"]) + " and data " + str(parsed_msg["lorawan_data"]))
            s = await FLIPClientSession.obtain(loop)
            s.talk(
                {
                    "event": "new_handler_response",
                    "ed_id": SIMULATED_DEVICE,
                    "lorawan_data": "placeholder_response"
                }
            )

        elif parsed_msg["event"] == "install_handler":
            global handled
            logger.log("LoRaWAN dispatcher", "Received data to handle device " + str(parsed_msg["ed_id"]) +
                       " with data " + str(parsed_msg["lorawan_data"]))
            handled = True
            
            # Adjust ed_data
            d = [x for x in ed_data if x["deveui"] == SIMULATED_DEVICE][0]
            d["handled"] = True
            

        elif parsed_msg["event"] == "device_data_from_handler":
            logger.log("LoRaWAN dispatcher", "Received app data from device " + str(parsed_msg["ed_id"]) + " with data " + str(parsed_msg["lorawan_data"]))


        else:
            self.talk({"error": "Unknown event!"})
            logger.error("LoRaWAN dispatcher", "Unknown event: " + parsed_msg["event"])


        self.end()

class FLIPClientSession(session.Session):
    def __init__(self, loop):
        super().__init__(loop, None)


    @staticmethod
    async def obtain(loop):
        s = FLIPClientSession(loop)
        await s.initiate()
        return s

    async def initiate(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setblocking(True)

        while True:
            try:
                sock.connect((config.HOST, config.FLIP_LOW_SERVER_PORT))
                logger.log("FLIP Client", "Connected to FLIP layer")
                break
            except ConnectionRefusedError as err:
                logger.error("FLIP client", "Cannot reach FLIP layer at " + config.HOST + ":" + str(config.FLIP_LOW_SERVER_PORT))
                await asyncio.sleep(3, self.loop)

        self.connection = sock

    def join_request(self, ed_id):
        self.talk({
            "event": "join_request",
            "ed_id": ed_id,
            "rssi": [x["rssi"] for x in ed_data if x["deveui"] == ed_id][0],
            "channel": 1,
            "sf": "SF7BW125",
            "lorawan_data": "placeholder"
        })
        self.end()

    def device_data(self, ed_id, data):
        self.talk({
            "event": "device_data_for_owner",
            "ed_id": ed_id,
            "lorawan_data": data
        })
        self.end()

class LoRaWANSpoof:

    def __init__(self):

        ## TCP port at which service is provided to LoRaWAN layer.
        self.flip_low_server_port = config.FLIP_LOW_SERVER_PORT

        ## TCP port at which service is provided by LoRaWAN layer.
        self.flip_lorawan_server_port = config.LORAWAN_SERVER_PORT

        self.loop = asyncio.get_event_loop()

        ## TCPListener that provides service to FLIP layer
        # Not supposed to bind to external IP address, but necessary for test setup
        self.flip_listener = TCPListener(self.loop, config.HOST, self.flip_lorawan_server_port)


    def run(self):

        asyncio.ensure_future(self.flip_listener.listen())
        asyncio.ensure_future(self.test_task())


        try:
            logger.log("LoRaWANSpoof", "Running")
            self.loop.run_forever()
        except KeyboardInterrupt:
            pass
        finally:
            logger.log("Gateway setup", "Going down")
            self.teardown()

    def teardown(self):
        self.loop.stop()
        self.loop.close()
        self.flip_listener.teardown()


    async def test_task(self):
        while True:
            await asyncio.sleep(35)
            if config_version in [0, 1, 9]: # 1 was in there as well originally
                s = await FLIPClientSession.obtain(self.loop)
                if handled:
                    s.device_data(SIMULATED_DEVICE, "[Fake lorawan stuff containing application data]")
                else:
                    s.join_request(SIMULATED_DEVICE)
                s.end()


class TCPListener:


    ## Initiate a TCP listener.
    # @param loop asyncio loop to operate on
    # @param host hostname to bind on
    # @param port port to bind on
    def __init__(self, loop, host, port):
        self.socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

        try:
            self.socket.bind((host, port))
        except Exception:
            logger.error("TCPListener", "Cannot bind to " + str(host) + ":" + str(port))
            raise

        self.loop = loop

    ## Asyncio coroutine that listens on self.port and spawns session.DispatcherSession to handle incoming requests
    # without blocking future requests.
    async def listen(self):

        self.socket.setblocking(False)
        self.socket.listen(20)

        while True:
            connection, _ = await self.loop.sock_accept(self.socket)
            connection.setblocking(True)

            dispatcher = LoRaWANDispatcherSession(self.loop, connection)
            asyncio.ensure_future(dispatcher.dispatch_message(self.loop))

    ## Releases listener resources
    def teardown(self):
        self.socket.close()

## Run a single gateway.
if __name__ == "__main__":
   lw = LoRaWANSpoof()
   lw.run()

