import asyncio
import socket
import math
import random
import flip_layer.session as session
import flip_layer.rest_api as api
import flip_layer.behaviour as behaviour
from flip_layer.logger import logger
from flip_layer.gateway_stub import GatewayStub
from flip_layer.stats import Stats
import os

import config

## Coordinator for FLIP upper layer. Keeps FLIP-related state, launches background tasks and handles communication with
#   lower LoRaWAN layer.
class Gateway(GatewayStub):

    ## Initialize gateway with initial state provided by config file.
    def __init__(self):

        super().__init__(config.HOST, config.CLUSTER_NAME, config.FLIP_HIGH_SERVER_PORT,
                         config.FLIP_HIGH_SERVER_LEADER_PORT, config.IS_LEADER)

        ## TCP port at which service is provided to LoRaWAN layer.
        self.flip_low_server_port = config.FLIP_LOW_SERVER_PORT

        ## TCP port at which service is provided by LoRaWAN layer.
        self.flip_lorawan_server_port = config.LORAWAN_SERVER_PORT

        self.loop = asyncio.get_event_loop()

        ## TCPListener that provides service to LoRaWAN layer
        self.lorawan_listener = TCPListener(self.loop, config.HOST, self.flip_low_server_port, self)

        # Periodic background tasks
        self.knn_task = behaviour.KNN(self)
        self.lorawan_poll_task = behaviour.LoRaWANPoll(self)

        self.ed_maintenance = behaviour.EDMaintenance(self)

        ## HTTP server to provide service to other FLIP gateways
        self.flip_api = api.FlipAPI()

        if self.is_leader:
            self.leader_api = api.LeaderAPI()

        self.stats = Stats(self)

        self.leader_only = config.LEADER_ONLY

        self.my_leader = None
        self.cluster_interconnect = None
        if self.is_leader:
            self.my_leader = self
            self.cluster_interconnect = behaviour.ClusterInterconnect(self)

        # TODO retrieve this from remote service
        self.cluster_id = config.CLUSTER_ID
        self.gateway_id = config.GATEWAY_ID

        ## Overview of end devices this gateway is concerned about, e.g.:
        # ed_data = {
        #     "owned_end_devices": ["dev" + str((config_version - 1) % config.NUM_END_DEVICES)],
        #     # "owned_end_devices": ["devEUI1", "devEUI2"],
        #     "handled_end_devices": handling,
        #     # "handled_end_devices": ["devEUI2", "devEUI3"],
        #     "heard_end_devices": {
        #         "dev" + str((config_version - 1) % config.NUM_END_DEVICES): {
        #             "rssi": (-50 if config_version == 1 else -128),
        #             "channel": 1,
        #             "sf": 7
        #         },
        #         "dev" + str(config_version): {
        #             "rssi": (-50 if config_version == 2 else -128),
        #             "channel": 2,
        #             "sf": 8
        #         },
        #         "dev" + str((config_version + 1) % config.NUM_END_DEVICES): {
        #             "rssi": (-50 if config_version == 0 else -128),
        #             "channel": 3,
        #             "sf": 9
        #         }
        #     }
        #   # SAME THING FOR heard_end_devices_dev_addr
        # }
        self.ed_data = None

        self.num_channels = config.NUM_CHANNELS


    ## Start running FLIP layer. Launches a TCPListener to handle service requests from LoRaWAN layer, a  rest_api.FlipAPI
    # HTTP server to handle communication between FLIP gateways and schedules background tasks for state maintenance.
    # All these activities are implemented as asyncio coroutines in order not to block the FLIP gateway process.
    def run(self):

        asyncio.ensure_future(self.lorawan_listener.listen())
        asyncio.ensure_future(self.knn_task.run())
        asyncio.ensure_future(self.knn_task.age_stubs())
        asyncio.ensure_future(self.lorawan_poll_task.run())

        asyncio.ensure_future(self.ed_maintenance.spread_subs())
        asyncio.ensure_future(self.ed_maintenance.maintenance())

        asyncio.ensure_future(self.stats.logging_task())
        self.flip_api.run(self)

        if self.is_leader:
            logger.log("Gateway setup", "Gateway will run leader API")
            asyncio.ensure_future(self.cluster_interconnect.discover_leaders())
            self.leader_api.run(self)

        try:
            logger.log("Gateway setup", "Running gateway")
            self.loop.run_forever()
        except KeyboardInterrupt:
            pass
        finally:
            logger.log("Gateway setup", "Going down")
            self.teardown()

    ## Handler for signal.SIGINT that properly releases resources held by gateway process.
    def teardown(self):
        logger.log("Gateway setup", "Shutting down gateway")
        self.loop.stop()
        self.loop.close()
        self.lorawan_listener.teardown()

    ## Return the kNN profile of the gateway represented by this stub.
    # Relies on local info.
    # @returns A device's kNN profile, e.g. {"dev1": 5, "dev2", 10}
    # @override
    def get_profile(self):
        result = {}
        if not (self.ed_data is None):
            for id, details in self.ed_data["heard_end_devices_dev_addr"].items():
                result[id] = details["rssi"]
        return result

    ## Calculate additional occupation for this gateway to handle a device attempting to heard at given rssi
    # @param rssi a number
    # @returns occupation a number
    # def occupation_from_rssi(self, rssi):
    #     # TODO MAX_RADIO RANGE in a real environment?
    #     distance = 10**(-((rssi-config.MAX_RSSI)/20))
    #     max_radio_range = 10**(-((config.MIN_RSSI-config.MAX_RSSI)/20))
    #     sf = 7 + int(distance / (max_radio_range/6))
    #     return self.occupation_from_sf(sf)

    ## Calculate additional occupation for this gateway to handle a device attempting to heard at given spreading factor
    # @param sf a number
    # @returns occupation a number
    def occupation_from_sf(self, sf):
        return 2 ** (sf - 7)

    # Calculate total occupation of running gateway if device with id joining_ed_id were to be handled by it.
    # Only call when end device with id joining_ed_id is being heard.
    # @param joining_ed_id
    # @returns occupation a number
    def calculate_occupation_on_handle(self, joining_ed_id):
        total_weight = self.occupation_from_sf(self.ed_data["heard_end_devices"][joining_ed_id]["sf"])

        for ed_id in self.ed_data["heard_end_devices_dev_addr"]:
            total_weight += self.occupation_from_sf(self.ed_data["heard_end_devices_dev_addr"][ed_id]["sf"])

        return total_weight

    # For each channel, calculate occupation.
    # @returns A list (config.NUM_CHANNELS entries) containing the occupation (a number) for each channel
    def calculate_channel_occupation(self):
        # TODO consistent channel definitions?
        channels = [0] * self.num_channels
        for ed_id in self.ed_data["heard_end_devices_dev_addr"]:
            try:
                channels[int(self.ed_data["heard_end_devices_dev_addr"][ed_id]["channel"])] += \
                    self.occupation_from_sf(self.ed_data["heard_end_devices_dev_addr"][ed_id]["sf"])
            except Exception as e:
                logger.error("Calculate channel occupation", "Error in computation: " + str(e))
        return channels


    # Calculate the best channel for an end_device with identifier ed_id to join on.
    # @param ed_id
    # @param channel_occupations a dict (gateway identifier -> channel_occupation) for neighbouring gateways.
    # @returns the channel number
    def calculate_best_channel(self, ed_id, channel_occupations):
        weight = self.occupation_from_sf(self.ed_data["heard_end_devices"][ed_id]["sf"])

        # Find channel(s) with lowest occupation rate
        my_channel_occupation = self.calculate_channel_occupation()

        # Steer away from default channels
        my_channel_occupation[0] = round(my_channel_occupation[0]*1.5)
        my_channel_occupation[1] = round(my_channel_occupation[1]*1.5)
        my_channel_occupation[2] = round(my_channel_occupation[2]*1.5)

        candidates = set([])
        minimum = min(my_channel_occupation)

        for channel, occupation in enumerate(my_channel_occupation):
            if occupation == minimum:
                candidates.add(channel)


        # Calc occupation of channel for environment
        base = [0]*self.num_channels
        for gw_id in channel_occupations:
            for index, val in enumerate(channel_occupations[gw_id]):
                base[index] += val


        #Calculate entropy
        total_occupation = sum(base) + weight
        #channel id -> entropy value
        entropies = {}

        for channel in candidates:
            base[channel] += weight
            entropy = 0.0
            for chan in base:
                if chan > 0:
                    frequency = float(chan) / total_occupation
                    entropy += -(frequency*math.log(frequency, 2))
            base[channel] -= weight
            entropies[channel] = entropy

        # Select candidate that maximises entropy
        best_channels = set([])
        best_entropy = max(entropies.values())
        for channel in candidates:
            if entropies[channel] == best_entropy:
                best_channels.add(channel)
        return random.choice(list(best_channels))

## TCP port listener to provide service to LoRaWAN layer.
# Spawns session.DispatcherSession to handle incoming request.
class TCPListener:

    ## Initiate a TCP listener.
    # @param loop asyncio loop to operate on
    # @param host hostname to bind on
    # @param port port to bind on
    def __init__(self, loop, host, port, gw):
        self.socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

        try:
            self.socket.bind((host, port))
        except Exception:
            logger.error("TCPListener", "Cannot bind to " + str(host) + ":" + str(port))
            raise


        self.loop = loop

        self.gw = gw

    ## Asyncio coroutine that listens on self.port and spawns session.DispatcherSession to handle incoming requests
    # without blocking future requests.
    async def listen(self):

        self.socket.setblocking(False)
        self.socket.listen(20)

        while True:
            connection, _ = await self.loop.sock_accept(self.socket)
            connection.setblocking(True)

            dispatcher = session.DispatcherSession(self.loop, connection, self.gw)
            asyncio.ensure_future(dispatcher.dispatch_message())

    ## Releases listener resources
    def teardown(self):
        self.socket.close()

## Run a single gateway.
if __name__ == "__main__":
   gw = Gateway()
   gw.run()

