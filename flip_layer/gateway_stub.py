from flip_layer.logger import logger, MsgColour
from time import time
import aiohttp

## A gateway somewhere in the FLIP federation.
# Provides the mechanisms by which that gateway can be reached.
class GatewayStub:
    ## Initiate a GatewayStub
    # @param host hostname at which the gateway can be reached
    # @param cluster_name name identifying the cluster to which the gateway belongs
    # @param flip_high_server_port TCP port at which the gateway provides FLIP service to other gateways
    # @param flip_high_server_leader_port TCP port at which the gateway provides FLIP cluster leader service to other gateways
    # @param is_leader indicates whether the gateway considers itself a cluster leader
    # @param profile A device's kNN profile, e.g. {"dev1": 5, "dev2", 10}
    # @param rps_timestamp time.time()-like timestamp for the RPS algorithm detailed in behaviour.KNN
    # @param maintaining_gw gateway.Gateway that is maintaining this stub
    def __init__(self, host, cluster_name, flip_high_server_port, flip_high_server_leader_port,
                 is_leader = False, profile = None, rps_timestamp = None, maintaining_gw = None, age = 0):

        self.maintaining_gw = maintaining_gw
        self.host = host
        self.cluster_name = cluster_name
        self.flip_high_server_port = flip_high_server_port
        self.flip_high_server_leader_port = flip_high_server_leader_port

        # Pick up timestamp if desired and already present in view
        if rps_timestamp is None:
            try:
                self.rps_timestamp  = self.maintaining_gw.knn_task.view[self.get_identifier()].rps_timestamp
            except (KeyError, AttributeError):
                self.rps_timestamp = float("-inf")
        else:
            self.rps_timestamp = rps_timestamp

        self.created_timestamp = time()

        ## Counter that indicates freshness of gateway info
        self.age = age

        # Never read profile directly, only do so through self.get_profile()
        if profile is None:
            self.__profile = {}
        else: # for demo purposes
            self.__profile = profile

        self.is_leader = is_leader

    @staticmethod
    def create_from_info(maintaining_gw, info, rps_timestamp = None):

        # Provide default values for optional info fields

        if "profile" in info:
            profile = info["profile"]
        else:
            profile = None
        if "age" in info:
            age = int(info["age"])
        else:
            age = None

        try:
            assert isinstance(info, dict)
            return GatewayStub(info["host"], info["cluster_name"], info["flip_high_server_port"], info["flip_high_server_leader_port"],
                               is_leader = info["is_leader"], profile = profile, maintaining_gw = maintaining_gw, rps_timestamp = rps_timestamp,
                               age = age)
        except Exception as exc:
            logger.error("Gateway stub", "Incorrect gateway representation")
            raise exc

    ## Return the info object detailing the gateway represented by this stub.
    # Relies on local info.
    # @returns A dict ("the info object")
    def get_info(self):
        return {
                "host": self.host,
                "flip_high_server_port": str(self.flip_high_server_port),
                "flip_high_server_leader_port": str(self.flip_high_server_leader_port),
                "cluster_name": self.cluster_name,
                "is_leader": self.is_leader,
                "profile": self.get_profile(),
                "age": self.age
            }

   ## Increment age of stub
    def increase_age(self):
        self.age += 1

    ## Return the identifier of the gateway represented by this stub.
    # Relies on local info.
    # @returns a string
    def get_identifier(self):
        return self.host + ":" + str(self.flip_high_server_port)

    @staticmethod
    def get_identifier_from_info(info):
        return info["host"] + ":" + info["flip_high_server_port"]

    ## Return the kNN profile of the gateway represented by this stub.
    # Relies on local info.
    # @preturns A device's kNN profile, e.g. {"dev1": 5, "dev2", 10}
    def get_profile(self):
        return self.__profile

    ## Update this stub's timestamp for the rps algorithm to the current time.
    def touch(self):
        self.rps_timestamp = time()

    # TODO Clean up below

    ## Stub rest_api.RandomPeerSampling.
    # @param info running gateway's info object
    # @param view info dictionary detailing gateways in RPS subset that will be pushed
    async def rps_update(self, info, view):
        async with aiohttp.ClientSession() as session:
            try:
                await session.post("http://" + self.host + ":" + str(self.flip_high_server_port) +"/api/rps_update",
                                       json={
                                           "info": info,
                                           "view": view
                                       })
            except aiohttp.client_exceptions.ClientConnectorError:
                raise

    ## Stub rest_api.Info.
    # @returns The info object provided by the gateway behind the stub.
    async def retrieve_info(self):
        async with aiohttp.ClientSession() as session:
            try:
                async with session.get("http://" + self.host + ":" + str(self.flip_high_server_port) + "/api/info") as response:
                    resp = await response.json()
                    return resp["info"]

            except aiohttp.client_exceptions.ClientConnectorError:
                raise


    ## Stub rest_api.KnnUpdate.
    # Automatically takes care of intra-cluster leader discovery - bit hacky
    # @param info running gateway's info object
    # @returns the info object provided by the gateway behind the stub
    # @returns a subview (cf. behaviour.KNN) of stubs the contacted stub wishes to share
    async def knn_update(self, info):
        message = {
            "info": info
        }
        if (not (self.maintaining_gw is None)) and (self.maintaining_gw.my_leader is None):
            message["req_leader"] = True
        async with aiohttp.ClientSession() as session:
            try:
                async with session.post("http://" + self.host + ":" + str(self.flip_high_server_port) +
                                        "/api/knn_update",
                                        json=message) as response:
                    resp = await response.json()
                    if (not (self.maintaining_gw is None)) and ("leader_info" in resp) and (self.maintaining_gw.my_leader is None):
                        candidate_leader = GatewayStub.create_from_info(self.maintaining_gw,resp["leader_info"])
                        try: #check whether alive to prevent dead GWs from lurking around
                            await candidate_leader.interconnect_info(self.maintaining_gw.get_identifier())
                            self.maintaining_gw.my_leader = candidate_leader
                            logger.log("Leader discovery", "Learned that " + candidate_leader.get_identifier() + " leads my cluster")
                        except Exception as e:
                            logger.log("Leader discovery", "Error: " + str(e))
                    return resp["info"], resp["view"]
            except aiohttp.client_exceptions.ClientConnectorError:
                raise

    ## Stub rest_api.LocalSubDissemination.
    # @param sub A subscription dictionary as in behaviour.EDMaintenance's create_sub() method.
    # @returns Response provided by the stub's LocalSubDissemination endpoint..
    async def local_sub(self, sub):
        async with aiohttp.ClientSession() as session:
            try:
                async with session.post("http://" + self.host + ":" + str(self.flip_high_server_port) + "/api/localsub",
                                   json=sub,
                                   timeout=None) as response:
                    response = await response.json()
                    return response
            except aiohttp.client_exceptions.ClientConnectorError:
                raise

    ## Stub rest_api.LocalConsensus.
    # @param message A consensus message as in behaviour.EDMaintenance's create_local_consensus_message_for_join() method.from
    # @returns Response provided by the stub's LocalConsensus endpoint.
    async def local_consensus(self, message):
        logger.log("FLIP-FLIP API", "Sending local consensus message to " + str(self.get_identifier()), MsgColour.FLIP_EVENT)
        async with aiohttp.ClientSession() as session:
            try:
                async with session.post("http://" + self.host + ":" + str(self.flip_high_server_port) + "/api/local_consensus",
                                   json=message,
                                   timeout=None) as response:
                    response = await response.json()
                    return response
            except aiohttp.client_exceptions.ClientConnectorError:
                raise

    # Stub rest_api.HandlerRequest.
    # @param join_message A handler message (cf. behaviour.EDMaintenance.create_handler_message()) to be provided to owner.
    async def handler_request(self, join_message):
        logger.log("FLIP-FLIP API", "Sending handler request (i.e. inform owner we are a candidate handler) to " + str(self.get_identifier()), MsgColour.FLIP_EVENT)
        async with aiohttp.ClientSession() as session:
            try:
                await session.post("http://" + self.host + ":" + str(self.flip_high_server_port) + "/api/handler_request",
                                   json=join_message,
                                   timeout=None)
            except aiohttp.client_exceptions.ClientConnectorError:
                raise

    # Stub rest_api.HandlerAccept.
    # @param handler_accept_message (cf. behaviour.EDMaintenance.create_handler_accept_message()) to be provided to handler.
    async def handler_accept(self, handler_accept_message):
        logger.log("FLIP-FLIP API", "Sending handler accept (i.e. inform candidate handler that it should handle) to " + \
                    str(self.get_identifier()), MsgColour.FLIP_EVENT)
        async with aiohttp.ClientSession() as session:
            try:
                await session.post("http://" + self.host + ":" + str(self.flip_high_server_port) + "/api/handler_accept",
                                   json=handler_accept_message,
                                   timeout=None)
            except aiohttp.client_exceptions.ClientConnectorError:
                raise

    # Stub rest_api.DeviceData.
    # @param device_data_message (cf. behaviour.EDMaintenance.create_device_data_message()) to be provided to owner.
    async def device_data(self, device_data_message):
        logger.log("FLIP-FLIP API", "Sending device data (i.e. inform owner of newly received data) to " + str(self.get_identifier()), MsgColour.FLIP_EVENT)
        async with aiohttp.ClientSession() as session:
            try:
                await session.post("http://" + self.host + ":" + str(self.flip_high_server_port) + "/api/device_data",
                                   json=device_data_message,
                                   timeout=None)
            except aiohttp.client_exceptions.ClientConnectorError:
                raise

    ## Stub rest_api.LeaderInfo.
    # @param identifier Identifier used to identify to remote for traceability reasons
    # @returns Response provided by the stub's LeaderInfo endpoint..
    async def leader_info(self, identifier):
        async with aiohttp.ClientSession() as session:
            try:
                async with session.post("http://" + self.host + ":" + str(self.flip_high_server_port) + "/api/leader_info",
                                   json={
                                       "identifier": identifier
                                   },
                                   timeout=None) as response:
                    if response.status != 200:
                        raise RuntimeError("No FLIP gateway at " + self.get_identifier() + " (HTTP status: " + str(response.status) +")")
                    response = await response.json()
                    return response
            except aiohttp.client_exceptions.ClientConnectorError:
                raise

    ## Stub rest_api.LeaderInfo.
    # @param identifier Identifier used to identify to remote for traceability reasons
    # @returns response provided by the stub's LeaderInfo endpoint..
    # @return elapsed time for connection
    async def interconnect_info(self, identifier):
        async with aiohttp.ClientSession() as session:
            try:
                async with session.post("http://" + self.host + ":" + str(self.flip_high_server_leader_port) + "/api/interconnect_info",
                                   json={
                                       "identifier": identifier
                                   },
                                   timeout=None) as response:
                    if response.status != 200:
                        raise RuntimeError("No leader at " + self.get_identifier() + " (HTTP status: " + str(response.status) +")")
                    response = await response.json()
                    return response
            except aiohttp.client_exceptions.ClientConnectorError:
                raise

    ## Stub rest_api.GlobalSubDissemination.
    # @param sub A subscription dictionary as in behaviour.EDMaintenance's create_sub() method.
    async def global_sub(self, msg):
        logger.log("FLIP-FLIP API", "Sending sub to leader " + str(self.get_identifier()), MsgColour.FLIP_EVENT)
        async with aiohttp.ClientSession() as session:
            try:
                async with session.post("http://" + self.host + ":" + str(self.flip_high_server_leader_port) + "/api/globalsub",
                                   json=msg,
                                   timeout=None) as response:
                    if response.status != 200:
                        raise RuntimeError("Leader API not found, status " + str(response.status))
            except aiohttp.client_exceptions.ClientConnectorError:
                raise

    def __str__(self):
        return "[Gateway at " + self.get_identifier() + "]"


