import asyncio
from flip_layer.logger import logger
from config import FLIP_STATS_LOG_INTERVAL
from math import floor

class Stats:
    def __init__(self, gw):


        self.gw = gw

        self.version = 0

        self.counters = {}

        self.counters["data_messages_received_from_lorawan"] = 0 #
        self.counters["data_messages_sent_to_owner"] = 0
        self.counters["data_messages_received_from_handler"] = 0

        self.counters["join_requests_received_from_lorawan"] = 0 #
        self.counters["consensuses_participated_in"] = 0 #
        self.counters["consensuses_won"] = 0 #
        self.counters["handlers_elected"] = 0 #
        self.counters["handler_installs"] = 0 #


        self.counters["sub_messages_received"] = 0 #
        self.counters["sub_messages_sent_for_owned_device"] = 0 #

        self.counters["knn_cycles_started"] = 0 #
        self.counters["knn_cycles_converged"] = 0 #

    def log(self):
        logger.log("Stats", "FLIP layer stats at version " + str(self.version))
        for k in self.counters:
            logger.log("Stats", k + " " + str(self.counters[k]))

        try:
            logger.log("Stats", "Channel occupation: " + str(self.get_histogram(self.gw.calculate_channel_occupation())))
            logger.log("Stats", "Channel occupation details: " + str(self.gw.calculate_channel_occupation()))
        except TypeError:
            pass

    @staticmethod
    def get_histogram(lst):
        if len(lst) == 0:
            return ""
        blocks = [u'▁', u'▂', u'▃', u'▄', u'▅', u'▆', u'▇', u'█']
        maxv = max(lst)
        result = [0] * len(lst)
        if maxv != 0:
            result = list(map(lambda x: round(float(x) / maxv * (len(blocks) - 1)), lst))
        result = list(map(lambda x: blocks[x], result))
        return "".join(result)

    async def logging_task(self):
        while True:
            self.log()
            await asyncio.sleep(FLIP_STATS_LOG_INTERVAL)
