from flip_layer.logger import logger, MsgColour
from tornado.web import RequestHandler
from tornado.platform.asyncio import AsyncIOMainLoop
from tornado.web import Application
from tornado.escape import json_decode
import asyncio
from flip_layer.gateway_stub import GatewayStub
from datetime import datetime
import random
import flip_layer.session as session

## Helper function returning a random subset of a behaviour.KNN's KNN and RPS set for dissemination purposes.
# @return a set
def get_dissemination_subset(gw):
    size_subset_knn = 1 if len(gw.knn_task.knn) == 1 else int(len(gw.knn_task.knn)/2)
    kset = set(random.sample(list(gw.knn_task.knn), size_subset_knn))
    rps_without_kset = list(gw.knn_task.rps - kset)
    rset = set(random.sample(rps_without_kset, min(len(rps_without_kset), int(len(gw.knn_task.rps)/2))))

    return kset.union(rset)

## Helper function to decide whether a message should be disseminate further
# @param answers collection of replies of gateways in current dissemination round ("new", "not new" or "connection error")
# @return a boolean indicating whether dissemination should halt
def dissemination_termination_condition(answers):
    for answer in answers:
        if answer == "new":
            return False
    return True


## Basic HTTP request handler.
class BaseHandler(RequestHandler):
    """
    Sets up headers for all resource handlers
    """

    def set_default_headers(self):
        """
        Sets up default headers for API classes. Allows all origins, and
        GET, POST, PUT, DELETE, OPTIONS methods.
        """
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers",
                        "x-requested-with, Content-Type")
        self.set_header('Access-Control-Allow-Methods',
                        'GET, POST, PUT, DELETE, OPTIONS')

    def options(self):
        """ """
        # no body
        self.set_status(204)
        self.finish()

## FLIP-FLIP API endpoint to retrieve a gateway's info object (over GET).
class Info(BaseHandler):

    def initialize(self, gateway_obj):
        self.gw = gateway_obj

    async def get(self):
        self.write({"info": self.gw.get_info()})
        self.write("\n")

## A BaseHandler with additional functionality to handle requests from gateways that send along their info.
class ViewUpdateHandler(BaseHandler):

    def initialize(self, gateway_obj):
        self.gw = gateway_obj

    ## Update view based on info object.
    # If info object details unknown gateway, add it to RPS set.
    def update_view_for_requestor(self, requestor_info):
        stub = self.gw.knn_task.update_view_with_info(requestor_info)
        return stub

## FLIP-FLIP API endpoint allowing remote gateways to pull a dictionary of gateway info objects detailing the kNN set of the running gateway.
class KnnUpdate(ViewUpdateHandler):
    async def post(self):
        body = json_decode(self.request.body)
        info = body["info"]
        logger.log("kNN update", "Received kNN update request from " + info["host"] + ":" +
                   info["flip_high_server_port"])
        self.update_view_for_requestor(info)

        view_dict = {id: self.gw.knn_task.view[id].get_info() for id in self.gw.knn_task.knn}

        response = {'info': self.gw.get_info(), 'view': view_dict}

        if ("req_leader" in body) and not (self.gw.my_leader is None):
            leader_info = self.gw.my_leader.get_info()
            del leader_info["profile"]
            response["leader_info"] = leader_info

        self.write(response)

## FLIP-FLIP API endpoint allowing remote gateways to push a dictionary of gateway info objects to the running gateway.
class RandomPeerSampling(ViewUpdateHandler):

    ## Receive info dictionary from remote gateway and schedule task to add them to own RPS set.
    async def post(self):
        body = json_decode(self.request.body)
        info = body["info"]
        s = self.update_view_for_requestor(info)
        s.touch()
        logger.log("RPS push", "Received RPS push from " + str(s))

        await asyncio.ensure_future(self.check_and_add_stubs(body["view"]))


    ## Coroutine that checks reachability of gateways detailed in an info dictionary, adds reachable ones to rps set in behaviour.KNN and prunes that set if it gets too big.
    # @param rps_info_dict dictionary of info objects to be considered.
    # TODO Checking is no longer done, update description and naming
    async def check_and_add_stubs(self, rps_info_dict):
        for id in rps_info_dict:
            s = self.gw.knn_task.update_view_with_info(rps_info_dict[id])
            logger.log("RPS push", "Received RPS push for "+str(s))
            self.gw.knn_task.rps.add(s.get_identifier())
            s.touch()
        self.gw.knn_task.prune_rps_set()

## FLIP-FLIP API endpoint for disseminating end device subscriptions within the local cluster.
class LocalSubDissemination(BaseHandler):

    def initialize(self, gateway_obj):
        self.gw = gateway_obj

    ## Accept and store subscription, further disseminate message if necessary.
    async def post(self):
        message = json_decode(self.request.body)

        logger.log("Local sub dissemination", "Received sub " + message["message_id"] + " for ed " + message["ed_id"] + \
                    " owned by " + message["owner_info"]["host"] + ":" + message["owner_info"]["flip_high_server_port"])

        # if new
        if message["message_id"] not in self.gw.ed_maintenance.sub_messages:
            self.gw.stats.counters["sub_messages_received"] += 1
            self.gw.ed_maintenance.sub_messages[message["message_id"]] = datetime.now()
            logger.log("Local sub dissemination", "Message with id " + message["message_id"] + " is new")
            self.gw.ed_maintenance.subs[message["ed_id"]] = message
            logger.log("Local sub dissemination", "Subs now: " + str(self.gw.ed_maintenance.subs.keys()))
            self.write({"message": "new"})
            self.finish()
        else:
            self.gw.ed_maintenance.sub_messages[message["message_id"]] = datetime.now()
            self.write({"message": "not new"})
            self.finish()
            return

        # message is new
        cold = False
        while not cold:
            answers = set([])
            subset = get_dissemination_subset(self.gw)
            logger.log("Local sub dissemination", "Dissemination subset " + str(subset))
            for gw_id in subset:
                try:
                    stub = self.gw.knn_task.view[gw_id]
                except KeyError:
                    logger.log("Local sub dissemination", "Skipped " + gw_id + "because it disappeared from view")
                    continue # KNN routine removed it
                try:
                    resp = await stub.local_sub(message)
                except Exception as e:
                    logger.error("Local sub dissemination", "Could not send sub to " + str(stub) + " because " + str(e))
                    resp = {"message" : "connection error"}
                answers.add(resp["message"])
            cold = dissemination_termination_condition(answers)
            await asyncio.sleep(1)

# FLIP-FLIP API endpoint for achieving local (intra-cluster) consensus on who should handle a specific end device.
class LocalConsensus(BaseHandler):

    def initialize(self, gateway_obj):
        self.gw = gateway_obj

    ## Accept and spread a consensus message (as defined in behaviour.EDMaintenance.create_local_consensus_message_for_join()).
    # Calls LocalConsensus.do_won_consensus() when consensus is reached.
    async def post(self):
        #TODO what if consensus is not achieved in time? What to do with nodes that are fast enough to "achieve consensus" before others realise they're involved?
        message = json_decode(self.request.body)
        logger.log("FLIP-FLIP API", "Received local consensus message from " + GatewayStub.get_identifier_from_info(message["sender_info"]),
                     MsgColour.FLIP_EVENT)
        logger.log("Local consensus", "Message " + message["message_id"])

        spread = False  # are followup actions required?
        response = None

        if message["ed_id"] in self.gw.ed_maintenance.consensus_messages:
            logger.log("Local consensus", "Message id: " + str(message["message_id"]) + " - known consensus message for end device " + \
                        message["ed_id"] + " at occ " + str(message["occupation"]))
            old_message = self.gw.ed_maintenance.consensus_messages[message["ed_id"]]
            old_message["timestamp"] = datetime.now()
            if old_message["occupation"] == message["occupation"]:
                response = {"message": "not new", "occupation": old_message["occupation"]}
            # already received a message with a better occupation rate
            elif old_message["occupation"] < message["occupation"]:
                response = {"message": "new", "occupation": old_message["occupation"]}
            # occupation rate of new message is better than previously received ones
            else:
                old_message["occupation"] = message["occupation"]
                #self.gw.ed_maintenance.consensus_messages[message["ed_id"]] = message
                response = {"message": "new", "occupation": message["occupation"]}
                spread = True

            # update channel occupation info
            old_message["channel_occupation"] = \
                {**(old_message["channel_occupation"]), **(message["channel_occupation"])}

        else:
            self.gw.stats.counters["consensuses_participated_in"] += 1
            logger.log("Local consensus", "Message id: " + str(message["message_id"]) + " - new consensus message for end device " + \
                        message["ed_id"] + " at occ " + str(message["occupation"]))
            # Copy to avoid passing timestamp
            m = message.copy()
            m["timestamp"] = datetime.now()
            self.gw.ed_maintenance.consensus_messages[message["ed_id"]] = m
            response = {"message": "new", "occupation": message["occupation"]}
            spread = True

        self.write(response)
        self.finish()

        # follow-up behaviour
        # did receive join?
        if message["ed_id"] in self.gw.ed_maintenance.join_attempts and spread:

            logger.log("Local consensus", "Message id: " + str(message["message_id"]) + \
                        " - wait before spreading (allow gateways to process join request before consensus)")
            await asyncio.sleep(1)

            promised_occ = self.gw.ed_maintenance.join_attempts[message["ed_id"]]["occupation"]
            cold = False
            while not cold:
                answers = set([])
                subset = get_dissemination_subset(self.gw)
                logger.log("Local consensus", "Message id: " + str(message["message_id"]) + " - propagating to " + str(subset))
                for stub_id in subset:
                    try:
                        # KNN might clean stuff up in the meantime
                        stub = self.gw.knn_task.view[stub_id]
                        response = await stub.local_consensus(message)
                    except Exception as e:
                        logger.error("Local consensus", "Message id: " + str(message["message_id"]) + " - connection error: " + str(e))
                        continue
                    if response["occupation"] < message["occupation"]:
                        message["occupation"] = response["occupation"]
                    answers.add(response["message"])
                cold = dissemination_termination_condition(answers)
                if not cold:
                    logger.log("Local consensus", "Message id: " + str(message["message_id"]) + " - not terminated: " + str(answers))
            logger.log("Local consensus", "Message id: " + str(message["message_id"]) + " - terminated on answers " + \
                        str(answers) + " and occupation " + str(message["occupation"]))
            if message["occupation"] == promised_occ:
                logger.log("Local consensus", "Message id: " + str(message["message_id"]) + " - I have the best score for ed " + \
                            message["ed_id"] + " with occ " + str(promised_occ))
                await self.do_won_consensus(message)

            else:
                logger.log("Local consensus", "Message id: " + str(message["message_id"]) + \
                           " - I do not have the best score for ed " + message["ed_id"])

    ## Notify owner that we have won the consensus.
    # @param won_message consensus message (cf. behaviour.EDMaintenance.create_local_consensus_message_for_join()) for
    # end device as stored by consensus winner
    async def do_won_consensus(self, won_message):
        ed_id = won_message["ed_id"]

        if not self.gw.ed_maintenance.join_attempts[ed_id]["did_win"]:
            self.gw.ed_maintenance.join_attempts[ed_id]["did_win"] = True
            self.gw.stats.counters["consensuses_won"] += 1
            handler_message = self.gw.ed_maintenance.create_handler_message(won_message)
            owner_info = self.gw.ed_maintenance.subs[ed_id]["owner_info"]
            logger.log("Local consensus", "Message id: " + str(won_message["message_id"]) + \
                       " - asking " + str(GatewayStub.get_identifier_from_info(owner_info)) + " whether I can handle " + \
                       str(handler_message["ed_id"]) + " on channel " + str(handler_message["channel"]))
            stub = GatewayStub.create_from_info(self.gw, owner_info, None)
            try:
                await stub.handler_request(handler_message)
            except Exception as e:
                logger.error("Local consensus", "Could not reach " + str(stub) + " because " + str(e))

## FLIP-FLIP API endpoint used by a gateway to inform an end device owner that it is a candidate for handling the owned device.
class HandlerRequest(BaseHandler):

    def initialize(self, gateway_obj):
        self.gw = gateway_obj

    ## Accepts and handles a handler message (as in behaviour.EDMaintenance.create_handler_message()).
    # Asks the LoRaWAN layer to provide a join ack to be sent back to a candidate handler.
    async def post(self):
        message = json_decode(self.request.body)
        ed_id = message["ed_id"]
        handler_candidate = GatewayStub.get_identifier_from_info(message["handler_info"])
        logger.log("FLIP-FLIP API", "Received handler request (i.e. owner is contacted by candidate handler) for " + message["ed_id"] + \
                   " at channel " + str(message["channel"]) + " from " + handler_candidate, MsgColour.FLIP_EVENT)

        if message["sub_message_id"] not in self.gw.ed_maintenance.held_subscriptions[message["ed_id"]]:
            logger.warn("Handler request", "Invalid message id for sub: " + message["sub_message_id"])
            return

        if not (ed_id in self.gw.ed_maintenance.ed_handler_candidates):
            self.gw.ed_maintenance.ed_handler_candidates[ed_id] = []
            self.gw.ed_maintenance.ed_handler_candidates[ed_id].append(message)
            logger.log("Handler request", "Waiting for other requests before choosing handler")
            await asyncio.sleep(2.5)
            # pick the best one
            winner_infos = None
            winner_occupation = float("inf")
            for join_message in self.gw.ed_maintenance.ed_handler_candidates[ed_id]:
                if join_message["occupation"] < winner_occupation:
                    winner_occupation = join_message["occupation"]
                    winner_infos = [join_message["handler_info"]]
                if join_message["occupation"] == winner_occupation:
                    winner_infos.append(join_message["handler_info"])

            self.gw.stats.counters["handlers_elected"] += 1
            winner = GatewayStub.create_from_info(self.gw, random.choice(winner_infos), None)
            logger.log("Handler request", "Chose " + str(winner) + " to handle device " + ed_id)

            self.gw.ed_maintenance.ed_handlers[ed_id] = winner
            del self.gw.ed_maintenance.ed_handler_candidates[ed_id]

            s = await session.LoRaWANClientSession.obtain(self.gw.loop, self.gw)
            logger.log("Handler request", "LoRaWAN data to be used for key generation: " + str(message["lorawan_data"]))
            s.new_handler(ed_id, message["lorawan_data"], message["channel"])

        else:
            self.gw.ed_maintenance.ed_handler_candidates[ed_id].append(message)
            return

## FLIP-FLIP API endpoint used by an end device owner to inform a candidate handler that it should handle a device, at the
#  same time providing it with join ack.
class HandlerAccept(BaseHandler):

    def initialize(self, gateway_obj):
        self.gw = gateway_obj

    ## Accepts and handles a handler accept message (cf. behaviour.EDMaintenance.create_handler_accept_message()).
    # Informs the LoRaWAN layer that it should handle the device, send the join ack and install the keys.
    async def post(self):
        message = json_decode(self.request.body)

        logger.log("FLIP-FLIP API", "Received handler accept (i.e. former candidate handler is contacted by owner) for " + message["ed_id"] + " from " + \
                   GatewayStub.get_identifier_from_info(message["owner_info"]), MsgColour.FLIP_EVENT)

        s = await session.LoRaWANClientSession.obtain(self.gw.loop, self.gw)
        s.install_handler(message["ed_id"], message["lorawan_data"])

## FLIP-FLIP API endpoint used by an end device handler to inform the corresponding owner of new application data.
class DeviceData(BaseHandler):

    def initialize(self, gateway_obj):
        self.gw = gateway_obj
        self.gw.stats.counters["data_messages_received_from_handler"] += 1

    ## Accepts and handles a device data message (cf. behaviour.EDMaintenance.create_device_data_message()), passing it
    # to the LoRaWAN layer for decryption.
    async def post(self):
        message = json_decode(self.request.body)

        logger.log("FLIP-FLIP API", "Received device data (i.e. current handler delivers data to owner) for device " + message["ed_id"] + \
                   " from " + GatewayStub.get_identifier_from_info(message["handler_info"]), MsgColour.FLIP_EVENT)

        s = await session.LoRaWANClientSession.obtain(self.gw.loop, self.gw)
        s.device_data_from_handler(message["ed_id"], message["lorawan_data"])

## FLIP-FLIP API endpoint for disseminating end device subscriptions within global deployment.
class GlobalSubDissemination(BaseHandler):

    def initialize(self, gateway_obj):
        self.gw = gateway_obj


    ## Prunes tree by removing irrelevant branches.
    # @param tree a dict <string -> list <string>> detailing next hops for every labeled in a graph
    # @param start node label (string) in graph for subtree that should remain
    # @param should_remain only to be used internally for recursive calls
    # @param depth only to be used interally for recursive calls
    def pruned_tree(self, tree, start, should_remain = None, depth = 0):

        result = tree.copy()

        if should_remain is None:
            should_remain = set()

        should_remain.add(start)

        if not (start in tree):
            return {}
        else:
            for neighbour in tree[start]:
                self.pruned_tree(tree, neighbour, should_remain, depth + 1)

        if depth == 0:
            for k in tree:
                if k not in should_remain:
                    del result[k]

            return result

        raise RuntimeError("Should be unreachable")



    ## Accept and store subscription, further disseminate message if necessary.
    async def post(self):
        message = json_decode(self.request.body)
        sub = message["sub"]
        tree = message["tree"]

        logger.log("Global sub dissemination", "Received sub " + sub["message_id"] + " for ed " + sub["ed_id"] + " owned by " + \
                    sub["owner_info"]["host"] + ":" + sub["owner_info"]["flip_high_server_port"])

        should_spread_locally = False

        if tree is None: #ONLY FOR FIRST HOP TO LOCAL LEADER
            tree = self.gw.cluster_interconnect.tree.copy()
        else:
            logger.log("Global sub dissemination", "Sub " + sub["message_id"] + " did not originate within own cluster, trigger local dissemination")
            #await self.gw.local_sub(sub)
            should_spread_locally = True

        if self.gw.cluster_name not in tree or len(tree[self.gw.cluster_name]) == 0:
            logger.log("Global sub dissemination", "Stub in tree, no further global dissemination of " + sub["message_id"])
        else:
            logger.log("Global sub dissemination", "Spreading sub " + sub["message_id"] + " along cluster tree " + str(tree))
            neighbours = tree[self.gw.cluster_name]
            for neighbour_name in neighbours:
                subtree = self.pruned_tree(tree,neighbour_name)
                logger.log("Global sub dissemination", "Sending sub " + sub["message_id"] + " to " + neighbour_name + " for spread along " + str(subtree))
                try:
                    await self.gw.cluster_interconnect.leaders[neighbour_name].global_sub({"sub": sub, "tree": subtree})
                except Exception as e:
                    logger.error("Global subs dissemination", "Could not reach neighbour in cluster tree: " + str(e))
                await asyncio.sleep(0.5)

        if should_spread_locally:
            await self.gw.local_sub(sub)

## FLIP-FLIP API endpoint used by remote leader to ask for leader info
class LeaderInfo(BaseHandler):

    def initialize(self, gateway_obj):
        self.gw = gateway_obj

    async def post(self):
        message = json_decode(self.request.body)

        logger.log("FLIP-FLIP API", "Remote gateway " + message["identifier"] +  " asked me about my leader")

        if self.gw.my_leader is None:
            self.write({"leader_info": None})
            logger.log
        else:
            leader_info = self.gw.my_leader.get_info()
            del leader_info["profile"]
            self.write({"leader_info": leader_info})
        self.finish()

## FLIP-FLIP API endpoint used to ask a remote leader about its connectivity to other leaders
class InterconnectInfo(BaseHandler):

    def initialize(self, gateway_obj):
        self.gw = gateway_obj

    async def post(self):
        message = json_decode(self.request.body)

        logger.log("FLIP-FLIP API", "Remote gateway " + message["identifier"] +  " asked me about my connectivity to other leaders")

        to_write = {}
        if self.gw.cluster_name in self.gw.cluster_interconnect.distances:
            to_write = self.gw.cluster_interconnect.distances[self.gw.cluster_name]
        self.write({"my_interconnect": to_write})
        self.finish()

## API client run by all gateways for local communications.
class FlipAPI:

    def run(self, gateway):

        # make sure tornado uses asyncio
        AsyncIOMainLoop().install()

        app = Application([
            (r'/api/info', Info, {'gateway_obj': gateway}),
            (r'/api/knn_update', KnnUpdate, {'gateway_obj': gateway}),
            (r'/api/rps_update', RandomPeerSampling, {'gateway_obj': gateway}),
            (r'/api/localsub', LocalSubDissemination, {'gateway_obj': gateway}),
            (r'/api/local_consensus', LocalConsensus, {'gateway_obj': gateway}),
            (r'/api/handler_request', HandlerRequest, {'gateway_obj': gateway}),
            (r'/api/handler_accept', HandlerAccept, {'gateway_obj': gateway}),
            (r'/api/device_data', DeviceData, {'gateway_obj': gateway}),
            (r'/api/leader_info', LeaderInfo, {'gateway_obj': gateway})
        ])

        # start tornado
        logger.log("API", "Started FLIP REST API")
        app.listen(gateway.flip_high_server_port, gateway.host)


# API client run by leader gateways for inter-cluster communications.
class LeaderAPI:

    def run(self, gateway):
        if gateway.leader_only:
            # make sure tornado uses asyncio
            AsyncIOMainLoop().install()

        app = Application([
            (r'/api/globalsub', GlobalSubDissemination, {'gateway_obj': gateway}),
            (r'/api/interconnect_info', InterconnectInfo, {'gateway_obj': gateway}),
        ])

        logger.log("Leader API", "Started leader REST API")
        app.listen(gateway.flip_high_server_leader_port, gateway.host)